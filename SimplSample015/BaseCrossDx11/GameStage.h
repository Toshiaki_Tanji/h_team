/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//----------------------------------------
	//クレジット
	//----------------------------------------
	class CreditStage : public Stage {
		//ビューの作成
		void CreateViewLight();
		//クレジットスプライトの作成
		void CreateCredit();

	public:
		//構築と破棄
		CreditStage() :Stage() {}
		virtual ~CreditStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};




	//--------------------------------------------------------------------------------------
	//	タイトル
	//--------------------------------------------------------------------------------------
	class TitleStage :public Stage {
		//ビュー作成
		//ビューとライトの作成
		void CreateViewLight();

		void CreateSprite();
		//フェーダー
		void CreateFader();

		float m_runTimer;
		//ボタンを押した
		bool PushBotton = false;

	public:


		//破棄と構築
		TitleStage() : Stage() {}
		virtual ~TitleStage() {}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

	};
	//--------------------------------------------------------------------------------------
	//	セレクト
	//--------------------------------------------------------------------------------------
	class SelectStage :public Stage {
		//ビュー作成
		//ビューとライトの作成
		void CreateViewLight();

		void CreateSprite();
		//フェーダー
		void CreateFader();

		//ステージのナンバーを格納
		int StageNo = 0;
		//ボタンが連続して反応しないように
		bool OnePushSelect = false;
		//ボタンを押した
		bool PushBotton = false;

	public:


		//破棄と構築
		SelectStage() : Stage() {}
		virtual ~SelectStage() {}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

	};
	//--------------------------------------------------------------------------------------
	//	リザルト
	//--------------------------------------------------------------------------------------
	class ResultStage :public Stage {
		//ビュー作成
		//ビューとライトの作成
		void CreateViewLight();
		void CreateSprite();
		//フェーダー
		void CreateFader();

		//ボタンを押した
		bool PushBotton = false;
		//ナンバーを格納
		int SelectNum = 0;

	public:
		//破棄と構築
		ResultStage() : Stage() {}
		virtual ~ResultStage() {}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

	};

	//--------------------------------------------------------------------------------------
	//	リザルト
	//--------------------------------------------------------------------------------------
	class GameOver :public Stage {
		//ビュー作成
		//ビューとライトの作成
		void CreateViewLight();
		void CreateSprite();
		//フェーダー
		void CreateFader();

		//ボタンを押した
		bool PushBotton = false;
		//ステージのナンバーを格納
		int SelectNum = 0;

	public:
		//破棄と構築
		GameOver() : Stage() {}
		virtual ~GameOver() {}
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

	};

}
//end basecross

