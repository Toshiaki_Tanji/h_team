/*!
@file Character.h
@brief キャラクターなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	//	class StaticModel : public GameObject;
	//	用途: 固定のモデル
	//--------------------------------------------------------------------------------------
	class StaticModel : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		wstring m_Resource;
	public:
		//構築と破棄
		StaticModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Resource
		);
		virtual ~StaticModel();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class DamegeObj : public GameObject;
	//	用途: 固定のモデル
	//--------------------------------------------------------------------------------------
	class DamegeObj : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		DamegeObj(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~DamegeObj();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class ShellBall : public GameObject;
	//	用途: 砲弾
	//--------------------------------------------------------------------------------------
	class Bomb : public GameObject {
		Vector3 m_StartPos;
		Vector3 m_NowScale;
		Vector3 m_JumpVec;
		Vector3 ExplodePoint;
		//vector<Vector3> ExplodePoint;
		float m_InStartTime;	//発射後の経過時間

		bool IsHitObj;

		shared_ptr< StateMachine<Bomb> >  m_StateMachine;	//ステートマシーン
	public:
		//構築と破棄
		Bomb(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos,
			const Vector3& JumpVec);
		virtual ~Bomb();
		//アクセサ
		shared_ptr< StateMachine<Bomb> > GetStateMachine() const {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//再初期化
		void Refresh(const Vector3& StartPos, const Vector3& JumpVec);
		//爆発を演出する関数
			//発射後一定の時間がたったら衝突をアクティブにする
		void HitTestCheckMotion();
	//地面についたかどうか
		bool IsArrivedBaseMotion();
		//爆発の開始
		void ExplodeStartMotion();
		//爆発の演出(演出終了で更新と描画を無効にする）
		void ExplodeExcuteMotion();


		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

	};

	//--------------------------------------------------------------------------------------
	//	class FiringState : public ObjState<ShellBall>;
	//	用途: 発射から爆発までのステート
	//--------------------------------------------------------------------------------------
	class FiringState : public ObjState<Bomb>
	{
		FiringState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<FiringState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Bomb>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Bomb>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Bomb>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class ExplodeState : public ObjState<ShellBall>;
	//	用途: 爆発最中のステート
	//--------------------------------------------------------------------------------------
	class ExplodeState : public ObjState<Bomb>
	{
		ExplodeState() {}
	public:
		//ステートのインスタンス取得 
		static shared_ptr<ExplodeState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Bomb>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Bomb>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Bomb>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	class Goal : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		//構築と破棄
		Goal(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~Goal();
		//初期化
		virtual void OnCreate() override;
	};

	//--------------------------------------------------------------------------------------
	//	頂点変更できるプレート
	//--------------------------------------------------------------------------------------
	class DynamicPlate : public GameObject {
	protected:
		//バックアップ頂点
		vector<VertexPositionNormalTexture> m_BackupVertices;
		//バックアップのインデックス（法線の計算に使う）
		vector<uint16_t> m_BackupIndices;
		//更新用の頂点
		vector<VertexPositionNormalTexture> m_UpdateVertices;
		//テクスチャリソース名
		wstring m_TextureResName;
		UINT m_WidthCount;
		UINT m_HeightCount;
		bool m_IsTile;	//タイリング
		Vector3 m_StartScale;
		Quaternion m_StartQt;
		Vector3 m_StartPosition;
		//ライト
		Light m_MyLight;
	protected:
		//構築と破棄
		DynamicPlate(const shared_ptr<Stage>& StagePtr,
			wstring TextureResName,
			UINT WidthCount,
			UINT HeightCount,
			bool IsTile,
			const Vector3& Scale,
			const Quaternion& Qt,
			const Vector3& Position
		);
		virtual ~DynamicPlate();
	public:
		//初期化
		virtual void OnCreate() override;
		virtual const Light& OnGetDrawLight()const override;

		//平面を返す
		PLANE GetPLANE() const;
		//判定用のRECTを返す
		COLRECT GetCOLRECT() const;
		//点との最近接点を返す
		void ClosestPtPoint(const Vector3& Point, Vector3& Ret);
		//点との距離を返す（戻り値がマイナスなら裏側）
		float GetDistPointPlane(const Vector3& Point) const;
		//ヒットテストをして最近接点と、Squareの法線を返す
		bool HitTestSphere(const SPHERE& Sp, Vector3& RetVec, Vector3& Normal);
	};

	//--------------------------------------------------------------------------------------
	//	タイリングする普通の壁
	//--------------------------------------------------------------------------------------
	class TileWall : public DynamicPlate {
		UINT m_WidthTileCount;
		UINT m_HeightTileCount;
	public:
		//構築と破棄
		TileWall(const shared_ptr<Stage>& StagePtr,
			wstring TextureResName,
			UINT WidthTileCount,
			UINT HeightTileCount,
			const Vector3& Scale,
			const Quaternion& Qt,
			const Vector3& Position
		);
		virtual ~TileWall();
		//初期化
		virtual void OnCreate() override;
	};
	//--------------------------------------------------------------------------------------
	//	ターゲットのスクエア
	//--------------------------------------------------------------------------------------
	class TargetSquare : public GameObject {
		//このオブジェクトのみで使用するスクエアメッシュ
		shared_ptr<MeshResource> m_SquareMeshResource;

		float Rolling;

	public:
		//構築と破棄
		TargetSquare(const shared_ptr<Stage>& StagePtr);
		virtual ~TargetSquare();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;
	};
	//--------------------------------------------------------------------------------------
	//	class ScoreItem : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	class ScoreItem : public GameObject {
		//ボックスの初期値
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		//構築と破棄
		ScoreItem(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~ScoreItem();
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class Ball : public GameObject;
	//	用途: 
	//--------------------------------------------------------------------------------------
	class BGball : public GameObject {
	public:
		//構築と破棄
		BGball(const shared_ptr<Stage>& StagePtr);
		virtual ~BGball();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};
}
//end basecross
