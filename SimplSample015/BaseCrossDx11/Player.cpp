﻿/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_MaxSpeed(2.0f),	//最高速度
		m_Decel(0.9f),	//減速値
		m_Mass(1.0f),	//質量
		m_IsOnScleMin(false)
	{}

	//初期化
	void Player::OnCreate() {
		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(0.25f, 0.25f, 0.25f);	//直径25センチの球体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		Ptr->SetPosition(0, 1.125f, 0);

		isBombThrow = true;

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		//PtrRedid->SetReflection(0.5f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();


		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();
		//反発
		PtrCol->SetIsHitAction(IsHitAction::AutoOnParentSlide);
		//PtrCol->SetDrawActive(true);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"jump");
		pMultiSoundEffect->AddAudioResource(L"Get");
		pMultiSoundEffect->AddAudioResource(L"Sting");


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"PLAYER_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTBoneModelDraw>();
		//auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"PLAYER_MESH");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		PtrDraw->AddAnimation(L"Walk", 0, 30, true, 30.0f);
		PtrDraw->AddAnimation(L"Ready",  40, 10, false, 30.0f);
		PtrDraw->AddAnimation(L"Throw", 50, 20, false, 30.0f);
		PtrDraw->AddAnimation(L"Jump", 90, 7, false, 30.0f);
		PtrDraw->AddAnimation(L"Landing",97, 12, false, 60.0f);
		PtrDraw->AddAnimation(L"Wait",120, 60, true, 30.0f);

		PtrDraw->ChangeCurrentAnimation(L"Wait");



		//透明処理
		SetAlphaActive(true);
		auto PtrCamera = dynamic_pointer_cast<LookAtCamera>(GetStage()->GetView()->GetTargetCamera());
		if (PtrCamera) {
			//LookAtCameraに注目するオブジェクト（プレイヤー）の設定
			PtrCamera->SetTargetObject(GetThis<GameObject>());
		}


		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Player> >(GetThis<Player>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(DefaultState::Instance());

	}

	//移動の向きを得る
	Vector3 Player::GetAngle() {
		Vector3 Angle(0, 0, 0);
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			auto PtrDraw = GetComponent<PNTBoneModelDraw>();
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			if (CntlVec[0].fThumbLX != 0 || CntlVec[0].fThumbLY != 0) {
				float MoveLength = 0;	//動いた時のスピード
				auto PtrTransform = GetComponent<Transform>();
				auto PtrCamera = GetStage()->GetView()->GetTargetCamera();
				//進行方向の向きを計算
				Vector3 Front = PtrTransform->GetPosition() - PtrCamera->GetEye();
				Front.y = 0;
				Front.Normalize();
				//進行方向向きからの角度を算出
				float FrontAngle = atan2(Front.z, Front.x);
				//コントローラの向き計算
				float MoveX = CntlVec[0].fThumbLX;
				float MoveZ = CntlVec[0].fThumbLY;
				//コントローラの向きから角度を計算
				float CntlAngle = atan2(-MoveX, MoveZ);
				//トータルの角度を算出
				float TotalAngle = FrontAngle + CntlAngle;
				//角度からベクトルを作成
				Angle = Vector3(cos(TotalAngle), 0, sin(TotalAngle));
				//正規化する
				Angle.Normalize();
				//Y軸は変化させない
				Angle.y = 0;

				if (PtrDraw->GetCurrentAnimation() == L"Wait") {
					PtrDraw->ChangeCurrentAnimation(L"Walk");
				}
			}
			else {
				if (PtrDraw->GetCurrentAnimation() == L"Walk") {
					PtrDraw->ChangeCurrentAnimation(L"Wait");
				}
			}
		}
		return Angle;
	}


	//更新
	void Player::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();

		//アニメーションを更新する
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		if (PtrDraw->UpdateAnimation(ElapsedTime) &&
			(PtrDraw->GetCurrentAnimation() == L"Landing" ||
				PtrDraw->GetCurrentAnimation() == L"Throw")) {
			PtrDraw->ChangeCurrentAnimation(L"Wait");
		}



		//デバッグ用
		auto PLPtr = GetComponent<Transform>();
		auto PLPos = PLPtr->GetPosition();
		auto SceneChange = -5.0f;
		if (PLPos.y < SceneChange) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGame");
		}
		//LIFE
		if (App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife <= 0) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameOver");
		}

	}
	void Player::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		
		auto GoalPtr = GetStage()->GetSharedObject(L"Goal");

		//相手の取得をして、縮む状態なら、それに合わせて少し余計に自分も下げてみる

		for (auto& v : OtherVec) {
			auto shScore = dynamic_pointer_cast<ScoreItem>(v);
			auto sh = dynamic_pointer_cast<GameObject>(v);
			auto transboxsh = dynamic_pointer_cast<TransformBox>(v);
			auto damagesh = dynamic_pointer_cast<DamegeObj>(v);

			if (v == GoalPtr)
			{
				if (App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_ScoreItem < 3)
				{

					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToResult");
					return;
				}
			}
			else if (v == shScore) {
				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Get", 0, 0.5f);
				App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_ScoreItem--;

				sh->SetDrawActive(false);
				sh->SetUpdateActive(false);
			}
			else if (v == damagesh) {

				App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife--;

				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Sting", 0, 0.5f);

				////相手のTransformを得る。
				//auto PtrOtherTrans = transboxsh->GetComponent<Transform>();
				////相手の場所を得る
				//auto OtherPos = PtrOtherTrans->GetPosition();

				////Transformを得る。
				//auto PtrTrans = GetComponent<Transform>();
				////場所を得る
				//auto Pos = PtrTrans->GetPosition();

				////飛ぶ方向を計算する
				//Pos -= OtherPos;
				//Pos.Normalize();
				//Pos.y = 0;
				//Pos *= 6.0f;
				//Pos += Vector3(0, 6.0f, 0);
				////重力を得る
				//auto PtrGravity = GetComponent<Gravity>();
				////ジャンプスタート
				//PtrGravity->StartJump(Pos);
			}

			else if (transboxsh) {
				if (transboxsh->GetStateMachine()->GetCurrentState() == ReturnState::Instance()) {
					m_IsOnScleMin = true;
				}
			}
		

		}
	}
	//ターンの最終更新時
	void Player::OnLastUpdate() {
		//if (m_IsOnScleMin) {
		//	auto TransPtr = GetComponent<Transform>();
		//	auto Pos = TransPtr->GetPosition();
		//	Pos.y -= 0.1f;
		//	TransPtr->SetPosition(Pos);
		//	m_IsOnScleMin = false;
		//}
	}

	//モーションを実装する関数群
	//移動して向きを移動方向にする
	void Player::MoveRotationMotion() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		Vector3 Angle = GetAngle();
		//Transform
		auto PtrTransform = GetComponent<Transform>();
		//Rigidbodyを取り出す
		auto PtrRedit = GetComponent<Rigidbody>();
		//現在の速度を取り出す
		auto Velo = PtrRedit->GetVelocity();
		//目的地を最高速度を掛けて求める
		auto Target = Angle * m_MaxSpeed;
		//目的地に向かうために力のかける方向を計算する
		//Forceはフォースである
		auto Force = Target - Velo;
		//yは0にする
		Force.y = 0;
		//加速度を求める
		auto Accel = Force; /// m_Mass;
		//ターン時間を掛けたものを速度に加算する
		Velo += (Accel /* ElapsedTime*/);
		//減速する
		//Velo *= m_Decel;
		//速度を設定する
		PtrRedit->SetVelocity(Velo);
		//回転の計算
		float YRot = PtrTransform->GetRotation().y;
		Quaternion Qt;
		Qt.Identity();
		if (Angle.Length() > 0.0f) {
			//ベクトルをY軸回転に変換
			float PlayerAngle = atan2(Angle.x, Angle.z);
			Qt.RotationRollPitchYaw(0, PlayerAngle, 0);
			Qt.Normalize();	
		}
		else {
			Qt.RotationRollPitchYaw(0, YRot, 0);
			Qt.Normalize();
		}
		//Transform
		PtrTransform->SetQuaternion(Qt);
	}


	//Aボタンでジャンプするどうかを得る
	bool Player::IsJumpMotion() {
		auto PtrGravity = GetComponent<Gravity>();
		if (PtrGravity->GetGravityVelocity().Length() <= 0.5f) {
			auto Trans = GetComponent<Transform>();
			auto Pos = Trans->GetPosition();
			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {
				//Aボタンが押された瞬間ならジャンプ
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
					Pos.y += 0.01f;
					Trans->SetPosition(Pos);
					auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"jump", 0, 0.5f);
					return true;
				}
			}
		}
		return false;
	}


	//Aボタンでジャンプする瞬間の処理
	void Player::JumpMotion() {
		auto PtrTrans = GetComponent<Transform>();
		//重力
		auto PtrGravity = GetComponent<Gravity>();
		//ジャンプスタート
		Vector3 JumpVec(0.0f, 4.5f, 0);
		PtrGravity->StartJump(JumpVec, 0);
		//アニメーションを変更する
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		PtrDraw->ChangeCurrentAnimation(L"Jump");
	}
	//Aボタンでジャンプしている間の処理
	//ジャンプ終了したらtrueを返す
	bool Player::JumpMoveMotion() {
		//重力
		auto PtrGravity = GetComponent<Gravity>();
		if (PtrGravity->GetGravityVelocity().Length() <= 0) {
			auto PtrDraw = GetComponent<PNTBoneModelDraw>();
			PtrDraw->ChangeCurrentAnimation(L"Landing");

			return true;
		}
		return false;
	}

	//ボタンでボムを投げるかどうかを得る
	bool Player::IsBombThrowMotion() {
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//ボタンが押された瞬間
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
				auto TargetPtr = GetStage()->GetSharedGameObject<TargetSquare>(L"Target");
				TargetPtr->SetDrawActive(true);
				isBombThrow = false;
				auto PtrDraw = GetComponent<PNTBoneModelDraw>();
				PtrDraw->ChangeCurrentAnimation(L"Ready");
				return true;

			}
			else if( CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_X) {
				isBombThrow = false;
				auto PtrDraw = GetComponent<PNTBoneModelDraw>();
				PtrDraw->ChangeCurrentAnimation(L"Ready");
				return true;
			}
		}
		return false;
	}
	//Bボタンを離して投げるまでの処理
	void Player::BombThrowMotion() {
		if (!isBombThrow) {//移動速度を抑えて投げる状態だと立ち止まるようにする
			auto Rigid = GetComponent<Rigidbody>();
			auto MoveVelo = Rigid->GetVelocity();
			MoveVelo.x *= 0.0f;
			MoveVelo.z *= 0.0f;
			Rigid->SetVelocity(MoveVelo);

			float ElapsedTime = App::GetApp()->GetElapsedTime();
			Vector3 Angle = GetAngle();
			//砲弾の追加
			auto PtrTrans = GetComponent<Transform>();
			//プレイヤーの向きを得る
			auto PlayerAngle = PtrTrans->GetRotation();

			//回転の計算
			float YRot = PtrTrans->GetRotation().y;
			Quaternion Qt;
			Qt.Identity();
			if (Angle.Length() > 0.0f) {
				//ベクトルをY軸回転に変換
				float PlayerAngle = atan2(Angle.x, Angle.z);
				Qt.RotationRollPitchYaw(0, PlayerAngle, 0);
				Qt.Normalize();
			}
			else {
				Qt.RotationRollPitchYaw(0, YRot, 0);
				Qt.Normalize();
			}
			//Transform
			PtrTrans->SetQuaternion(Qt);


			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {
				//Aボタンが押された瞬間
				if (CntlVec[0].wReleasedButtons & XINPUT_GAMEPAD_B) {
					//ボムを投げた
					isBombThrow = true;
					auto TargetPtr = GetStage()->GetSharedGameObject<TargetSquare>(L"Target");
					TargetPtr->SetDrawActive(false);

					auto PtrDraw = GetComponent<PNTBoneModelDraw>();
					PtrDraw->ChangeCurrentAnimation(L"Throw");

					auto NowPos = PtrTrans->GetPosition();
					NowPos.x += sin(PlayerAngle.y) / 10;
					NowPos.y += 0.25f;
					NowPos.z += cos(PlayerAngle.y) / 10;
					Vector3 ShellSpeed;

					////↑向き
					//if (PlayerAngle.y < PIDIV8 && PlayerAngle.y > -PIDIV8) {
					//	ShellSpeed = Vector3(sin(0.0f), 0.0f, cos(0.0f));
					//}
					////↗向き
					//else if (PlayerAngle.y < PI3DIV8 && PlayerAngle.y > PIDIV8) {
					//	ShellSpeed = Vector3(sin(XM_PIDIV4), 0.0f, cos(XM_PIDIV4));
					//}
					////→向き
					//else if (PlayerAngle.y < PI5DIV8 && PlayerAngle.y > PI3DIV8) {
					//	ShellSpeed = Vector3(sin(XM_PIDIV2), 0.0f, cos(XM_PIDIV2));
					//}
					////↘向き
					//else if (PlayerAngle.y < PI7DIV8 && PlayerAngle.y > PI5DIV8) {
					//	ShellSpeed = Vector3(sin(PI3DIV4), 0.0f, cos(PI3DIV4));
					//}
					////↓向き
					//else if (PlayerAngle.y < -PI7DIV8 || PlayerAngle.y > PI7DIV8) {
					//	ShellSpeed = Vector3(sin(XM_PI), 0.0f, cos(XM_PI));
					//}
					////↙向き
					//else if (PlayerAngle.y > -PI7DIV8 && PlayerAngle.y < -PI5DIV8) {
					//	ShellSpeed = Vector3(sin(-PI3DIV4), 0.0f, cos(-PI3DIV4));
					//}
					////←向き
					//else if (PlayerAngle.y > -PI5DIV8 && PlayerAngle.y < -PI3DIV8) {
					//	ShellSpeed = Vector3(sin(-XM_PIDIV2), 0.0f, cos(-XM_PIDIV2));
					//}
					////↖向き
					//else if (PlayerAngle.y > -PI3DIV8 && PlayerAngle.y < -PIDIV8) {
					//	ShellSpeed = Vector3(sin(-XM_PIDIV4), 0.0f, cos(-XM_PIDIV4));
					//}
					////例外があった場合
					//else {
					ShellSpeed = Vector3(sin(PlayerAngle.y), 0.0f, cos(PlayerAngle.y));
					//}

					ShellSpeed *= 3.0f;
					Vector3 Velo = GetComponent<Rigidbody>()->GetVelocity();
					Velo.y = 0;
					//移動スピードを加算
					ShellSpeed += Velo;
					//打ち上げの上向きの初速度を追加（値は固定）
					ShellSpeed += Vector3(0.0f, 3.0f, 0);
					//グループ内に空きがあればそのオブジェクトを再利用
					//そうでなければ新規に作成
					auto Group = GetStage()->GetSharedObjectGroup(L"ShellBallGroup");
					auto ShellVec = Group->GetGroupVector();
					for (auto Ptr : ShellVec) {
						//Ptrはweak_ptrなので有効性チェックが必要
						if (!Ptr.expired()) {
							auto ShellPtr = dynamic_pointer_cast<Bomb>(Ptr.lock());
							if (ShellPtr) {
								if ((!ShellPtr->IsUpdateActive()) && (!ShellPtr->IsDrawActive())) {
									ShellPtr->Refresh(NowPos, ShellSpeed);
									return;
								}
							}
						}
					}
					//ここまで来たら空きがなかったことになる
					//砲弾の追加
					auto Sh = GetStage()->AddGameObject<Bomb>(NowPos, ShellSpeed);
					//グループに追加
					Group->IntoGroup(Sh);
				}
				if (CntlVec[0].wReleasedButtons & XINPUT_GAMEPAD_X) {
					//ボムを投げた
					isBombThrow = true;

					auto PtrDraw = GetComponent<PNTBoneModelDraw>();
					PtrDraw->ChangeCurrentAnimation(L"Throw");

					auto NowPos = PtrTrans->GetPosition();
					NowPos.x += sin(PlayerAngle.y) / 2;
					NowPos.z += cos(PlayerAngle.y) / 2;
					Vector3 ShellSpeed;
					//打ち上げの上向きの初速度を追加（値は固定）
					ShellSpeed += Vector3(0.0f, 0.0f, 0);
					//グループ内に空きがあればそのオブジェクトを再利用
					//そうでなければ新規に作成
					auto Group = GetStage()->GetSharedObjectGroup(L"ShellBallGroup");
					auto ShellVec = Group->GetGroupVector();
					for (auto Ptr : ShellVec) {
						//Ptrはweak_ptrなので有効性チェックが必要
						if (!Ptr.expired()) {
							auto ShellPtr = dynamic_pointer_cast<Bomb>(Ptr.lock());
							if (ShellPtr) {
								if ((!ShellPtr->IsUpdateActive()) && (!ShellPtr->IsDrawActive())) {
									ShellPtr->Refresh(NowPos, ShellSpeed);
									return;
								}
							}
						}
					}
					//ここまで来たら空きがなかったことになる
					//砲弾の追加
					auto Sh = GetStage()->AddGameObject<Bomb>(NowPos, ShellSpeed);
					//グループに追加
					Group->IntoGroup(Sh);
				}

			}
		}
	}
	//間の処理
	//終了したらtrueを返す
	bool Player::BombThrowEndMotion() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//ボムが投げられた
		if (isBombThrow) {
			CoolTime += ElapsedTime;
			if (CoolTime > 0.5f) {
				CoolTime = 0.0f;
				return true;
			}
		}
		//投げられていない
		return false;
	}
	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DefaultState> DefaultState::Instance() {
		static shared_ptr<DefaultState> instance;
		if (!instance) {
			instance = shared_ptr<DefaultState>(new DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DefaultState::Enter(const shared_ptr<Player>& Obj) {
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DefaultState::Execute(const shared_ptr<Player>& Obj) {
		Obj->MoveRotationMotion();
		if (Obj->IsJumpMotion()) {
			//Jumpボタンでステート変更
			Obj->GetStateMachine()->ChangeState(JumpState::Instance());
		}
		if (Obj->IsBombThrowMotion()) {
			Obj->GetStateMachine()->ChangeState(BombThrow::Instance());
		}

	}
	//ステートにから抜けるときに呼ばれる関数
	void DefaultState::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class JumpState : public ObjState<Player>;
	//	用途: ジャンプ状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<JumpState> JumpState::Instance() {
		static shared_ptr<JumpState> instance;
		if (!instance) {
			instance = shared_ptr<JumpState>(new JumpState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void JumpState::Enter(const shared_ptr<Player>& Obj) {
		//ジャンプ中も移動可能とする
		Obj->MoveRotationMotion();
		Obj->JumpMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void JumpState::Execute(const shared_ptr<Player>& Obj) {
		//ジャンプ中も移動可能とする
		Obj->MoveRotationMotion();
		if (Obj->JumpMoveMotion()) {
			//通常状態に戻る
			Obj->GetStateMachine()->ChangeState(DefaultState::Instance());
		}
		if (Obj->IsBombThrowMotion()) {
			Obj->GetStateMachine()->ChangeState(BombThrow::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void JumpState::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class BombThrow : public ObjState<Player>;
	//	用途: 爆弾投げる状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<BombThrow> BombThrow::Instance() {
		static shared_ptr<BombThrow> instance;
		if (!instance) {
			instance = shared_ptr<BombThrow>(new BombThrow);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void BombThrow::Enter(const shared_ptr<Player>& Obj) {
	
	}
	//ステート実行中に毎ターン呼ばれる関数
	void BombThrow::Execute(const shared_ptr<Player>& Obj) {
		//ボムを呼び出し中
		Obj->BombThrowMotion();
		//投げ終わったとき通常に戻る
		if (Obj->BombThrowEndMotion()) {
			Obj->GetStateMachine()->ChangeState(DefaultState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void BombThrow::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}


	//プレイヤーの体力
	LIFE::LIFE(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos, const size_t& LayerNum, const UINT& HPLen
		) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_LayerNum(LayerNum),
		m_HPLen(HPLen)
	{}
	LIFE::~LIFE() {}

	//初期化
	void LIFE::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横1個ずつ表示)
		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vector3(-HelfSize, HelfSize, 0),Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(-HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 1.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1, 1.0f), Vector2(1.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
		SetDrawLayer(m_LayerNum);
	}
	void LIFE::OnUpdate() {
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		if (m_HPLen > App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife && m_TextureKey == L"LIFE_TX") {
			PtrDraw->SetTextureResource(L"LIFELOSE_TX");
		}
	}

}
//end basecross

