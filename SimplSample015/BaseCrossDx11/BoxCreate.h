/*!
@file BoxCreate.h
@brief ステージに配置するボックスなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class TransformBox : public GameObject;
	//	用途: ボックス
	//--------------------------------------------------------------------------------------
	class TransformBox : public GameObject {
		//ボックスの初期値
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

		Vector3 m_ScaleDefault;
		wstring m_NowTransState;
		Vector3 m_LatestHitPos;
		float ChangeTimer;
		bool IsTransed;
		bool IsUpper;
		bool IsNSside;
		bool IsWEside;
		shared_ptr< StateMachine<TransformBox> >  m_StateMachine;	//ステートマシーン
	public:
		//構築と破棄
		TransformBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~TransformBox();
		//アクセサ
		shared_ptr< StateMachine<TransformBox> > GetStateMachine() const {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//演出する関数
		void SetHitPos(Vector3 hitpos);
		void TransformMotion();
		void ReturnMotion();
		bool IsReturned();
	};
	//--------------------------------------------------------------------------------------
	//	class State : public ObjState<TransformBox>;
	//	用途: 通常ステート
	//--------------------------------------------------------------------------------------
	class OriginState : public ObjState<TransformBox>
	{
		OriginState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<OriginState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TransformBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TransformBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TransformBox>& Obj)override;
	};
	//--------------------------------------------------------------------------------------
	//	class TransformState : public ObjState<TransformBox>;
	//	用途: ステート
	//--------------------------------------------------------------------------------------
	class TransUPState : public ObjState<TransformBox>
	{
		TransUPState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<TransUPState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TransformBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TransformBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TransformBox>& Obj)override;
	};
	//--------------------------------------------------------------------------------------
	//	class ReturnState : public ObjState<TransformBox>;
	//	用途: 元に戻るステート
	//--------------------------------------------------------------------------------------
	class ReturnState : public ObjState<TransformBox>
	{
		ReturnState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ReturnState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TransformBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TransformBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TransformBox>& Obj)override;
	};
	//--------------------------------------------------------------------------------------
	//	class NormalBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	class NormalBox : public GameObject {
		//ボックスの初期値
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;

	public:
		//構築と破棄
		NormalBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~NormalBox();
		//初期化
		virtual void OnCreate() override;

	};

}
//end basecross