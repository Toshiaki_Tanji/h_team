/*!
@file Scene.h
@brief シーン
*/
#pragma once

#include "stdafx.h"

namespace basecross{

	struct GameParamaters {
		//プレイヤーのライフ
		UINT m_PlayerLife;
		//スコアアイテムの数
		UINT m_ScoreItem;
		//吹き出しを表示させるタスク
		bool m_BalloonOpenTask;
		//頭のタスク
		bool m_IsHeadTaskClear;

		GameParamaters() :
			m_PlayerLife(),
			m_ScoreItem(),
			m_BalloonOpenTask(false),
			m_IsHeadTaskClear(false)
		{}
	};

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase{
		//リソース登録
		shared_ptr<CResource> m_resource;
		//BGMの設定
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		//bgm
		wstring m_Prebgm;

		//パラメータ
		GameParamaters m_GameParamaters;

	public:
		Scene() :SceneBase(), m_GameParamaters() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		*/
		//--------------------------------------------------------------------------------------
		//Scene() :SceneBase(){}
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Scene(){}
		//--------------------------------------------------------------------------------------
		/*!
		@brief ゲームパラメータを得る
		@return	ゲームパラメータ
		*/
		//--------------------------------------------------------------------------------------
		GameParamaters& GetGameParamaters() {
			return m_GameParamaters;
		}
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//イベント取得
		virtual void OnEvent(const shared_ptr<Event>& event)override;

	};

}

//end basecross
