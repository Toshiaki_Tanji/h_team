/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {


	//--------------------------------------------------------------------------------------
	//	class StaticModel : public GameObject;
	//	用途: 固定のモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	StaticModel::StaticModel(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Resource
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_Resource(Resource)
	{
	}
	StaticModel::~StaticModel() {}

	//初期化
	void StaticModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::Slide);
		PtrCol->SetFixed(true);
		//PtrCol->SetDrawActive(true);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(2.0f, 0.2f, 2.0f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(m_Resource);
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(m_Resource);
		PtrDraw->SetDepthStencilState(DepthStencilState::Read); // スプライト多いモデルなどはこれを適用
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->SetLighting(ShaderLighting::Midium);
		PtrDraw->SetEmissive(Color4(0.4, 0.4, 0.4, 0));
		PtrDraw->SetDiffuse(Color4(0.6, 0.6, 0.6, 1));
		SetAlphaActive(true);
		SetDrawLayer(1);

	}

	//--------------------------------------------------------------------------------------
	//	class DamegeObj : public GameObject;
	//	用途: 固定のモデル
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DamegeObj::DamegeObj(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	DamegeObj::~DamegeObj() {}

	//初期化
	void DamegeObj::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::Slide);
		PtrCol->SetDrawActive(true);
		PtrCol->SetFixed(true);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.5f, 0.5f, 0.5f),
			Vector3(0.0f, 0, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
		);


		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"MALLON_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"MALLON_MESH");
		//PtrDraw->SetDepthStencilState(DepthStencilState::Read); // スプライト多いモデルなどはこれを適用
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->SetLighting(ShaderLighting::Midium);
		PtrDraw->SetEmissive(Color4(0.4, 0.4, 0.4, 0));
		PtrDraw->SetDiffuse(Color4(0.6, 0.6, 0.6, 1));
		SetAlphaActive(true);

	}

	//--------------------------------------------------------------------------------------
	//	class Bomb : public GameObject;
	//	用途: 砲弾
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Bomb::Bomb(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos,
		const Vector3& JumpVec) :
		GameObject(StagePtr), m_StartPos(StartPos),
		m_NowScale(0.25f, 0.25f, 0.25f), m_JumpVec(JumpVec)
	{}
	Bomb::~Bomb() {}
	//初期化
	void Bomb::OnCreate() {
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(m_NowScale);
		Ptr->SetRotation(0, 0, 0.0f);
		Ptr->SetPosition(m_StartPos);
		IsHitObj = false;

		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		//PtrGravity->SetBaseY(0.125f);
		//ジャンプスタート
		PtrGravity->StartJump(m_JumpVec);
		//影の作成
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形状
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");


		auto PtrCol = AddComponent<CollisionSphere>();
		PtrCol->SetIsHitAction(IsHitAction::Slide);
		PtrCol->SetUpdateActive(false);

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"bakuha");

		//描画コンポーネント
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//メッシュの登録
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");

		PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 1.0f, 0.5f));

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Bomb> >(GetThis<Bomb>());
		//最初のステートをFiringStateに設定
		m_StateMachine->SetCurrentState(FiringState::Instance());
		//FiringStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<Bomb>());

	}

	void Bomb::Refresh(const Vector3& StartPos, const Vector3& JumpVec) {
		SetUpdateActive(true);
		SetDrawActive(true);
		m_StartPos = StartPos;
		m_JumpVec = JumpVec;
		m_InStartTime = 0;
		IsHitObj = false;

		//Transform取得
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_NowScale);
		Ptr->SetPosition(m_StartPos);

		//重力をつける
		auto PtrGravity = GetComponent<Gravity>();
		PtrGravity->SetUpdateActive(true);
		//サウンドを登録.
		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"bakuha");

		//描画コンポーネント
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 1.0f, 0.5f));

		auto PtrCol = GetComponent<CollisionSphere>();
		PtrCol->SetIsHitAction(IsHitAction::Slide);
		PtrCol->SetFixed(false);
		PtrCol->SetUpdateActive(false);

		//ジャンプスタート
		PtrGravity->StartJump(m_JumpVec);
		//影の作成
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形状
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		//今のステートをFiringStateに設定
		m_StateMachine->SetCurrentState(FiringState::Instance());
		//FiringStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<Bomb>());

	}


	void Bomb::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();
		//ステートマシンを使うことでUpdate処理を分散できる		

	}


	//爆発を演出する関数
	//発射後一定の時間がたったら衝突をアクティブにする
	void Bomb::HitTestCheckMotion() {
		//衝突判定を呼び出す
		auto PtrCollision = GetComponent<CollisionSphere>();
		if (PtrCollision->IsUpdateActive()) {
			//既に衝突は有効
			return;
		}
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_InStartTime += ElapsedTime;
		if (m_InStartTime > 0.05f) {
			//衝突を有効にする
			PtrCollision->SetUpdateActive(true);
		}
	}


	void Bomb::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		IsHitObj = true;
		for (auto &v : OtherVec) {
			auto ThisPos = GetComponent<Transform>()->GetPosition();

			auto shTransBox = dynamic_pointer_cast<TransformBox>(v);
			auto sh = dynamic_pointer_cast<GameObject>(v);
			if (shTransBox) {
				shTransBox->SetHitPos(ThisPos);
				shTransBox->GetStateMachine()->ChangeState(TransUPState::Instance());
			}
			return;
		}
	}


	//爆発を演出する関数
	bool Bomb::IsArrivedBaseMotion() {
		if (IsHitObj) {
			//	//落下速度が0ならtrue
			IsHitObj = false;
			auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"bakuha", 0, 0.5f);
			return true;
		}
		return false;
	}


	//爆発の開始
	void Bomb::ExplodeStartMotion() {
		//Transform取得
		auto Ptr = GetComponent<Transform>();
		auto PtrPos = Ptr->GetPosition();

		auto Col = GetComponent<CollisionSphere>();
		Col->SetUpdateActive(false);
		Col->SetFixed(true);


		m_NowScale = Vector3(1.5f, 1.5f, 1.5f);
		Ptr->SetScale(m_NowScale);
		//描画コンポーネント
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		//爆発中は赤
		PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 1.0f, 1.0f));
	}

	//爆発の演出(演出終了で更新と描画を無効にする）
	void Bomb::ExplodeExcuteMotion() {
		//Transform取得
		auto Ptr = GetComponent<Transform>();

		m_NowScale *= 0.85f;
		if (m_NowScale.x < 0.5f) {
			m_NowScale = Vector3(0.2f, 0.2f, 0.2f);
			//表示と更新をしないようにする
			//こうするとこの後、更新及び描画系は全く呼ばれなくなる
			//再び更新描画するためには、外部から操作が必要（プレイヤーが呼び起こす）
			SetDrawActive(false);
			SetUpdateActive(false);
			RemoveComponent<Shadowmap>();
			ExplodePoint = Vector3(0.0f, 0.0f, 0.0f);

		}
		Ptr->SetScale(m_NowScale);
	}


	//--------------------------------------------------------------------------------------
	//	class FiringState : public ObjState<Bomb>;
	//	用途: 発射から爆発までのステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<FiringState> FiringState::Instance() {
		static shared_ptr<FiringState> instance;
		if (!instance) {
			instance = shared_ptr<FiringState>(new FiringState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void FiringState::Enter(const shared_ptr<Bomb>& Obj) {
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void FiringState::Execute(const shared_ptr<Bomb>& Obj) {

		//経過時間によって衝突を有効にする
		Obj->HitTestCheckMotion();

		//落下終了かどうかチェック
		if (Obj->IsArrivedBaseMotion()) {
			//落下終了ならステート変更
			Obj->GetStateMachine()->ChangeState(ExplodeState::Instance());
		}

	}
	//ステートにから抜けるときに呼ばれる関数
	void FiringState::Exit(const shared_ptr<Bomb>& Obj) {
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class ExplodeState : public ObjState<Bomb>;
	//	用途: 爆発最中のステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ExplodeState> ExplodeState::Instance() {
		static shared_ptr<ExplodeState> instance;
		if (!instance) {
			instance = shared_ptr<ExplodeState>(new ExplodeState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ExplodeState::Enter(const shared_ptr<Bomb>& Obj) {
		//爆発の開始
		Obj->ExplodeStartMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void ExplodeState::Execute(const shared_ptr<Bomb>& Obj) {
		//爆発演出の実行
		Obj->ExplodeExcuteMotion();
	}
	//ステートにから抜けるときに呼ばれる関数
	void ExplodeState::Exit(const shared_ptr<Bomb>& Obj) {
		//何もしない
	}



	//--------------------------------------------------------------------------------------
	//	class  : public GameObject;
	//	用途: 
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Goal::Goal(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	Goal::~Goal() {}

	//初期化
	void Goal::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		//PtrObb->SetDrawActive(true);
		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.25f, 0.5f, 0.25f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"HOUSE_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"HOUSE_MESH");
		//PtrDraw->SetDepthStencilState(DepthStencilState::Read); // スプライト多いモデルなどはこれを適用
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->SetLighting(ShaderLighting::Midium);
		PtrDraw->SetEmissive(Color4(1.0, 1.0, 1.0, 0));
		PtrDraw->SetDiffuse(Color4(1.0, 1.0, 1.0, 1));
		SetAlphaActive(true);


	}
	//--------------------------------------------------------------------------------------
	//	頂点変更できるプレート
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DynamicPlate::DynamicPlate(const shared_ptr<Stage>& StagePtr,
		wstring TextureResName,
		UINT WidthCount,
		UINT HeightCount,
		bool IsTile,
		const Vector3& Scale,
		const Quaternion& Qt,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_TextureResName(TextureResName),
		m_WidthCount(WidthCount),
		m_HeightCount(HeightCount),
		m_IsTile(IsTile),
		m_StartScale(Scale),
		m_StartQt(Qt),
		m_StartPosition(Position),
		m_MyLight()
	{
		Matrix4X4 mt;
		mt.DefTransformation(Scale, Qt, Position);
		Vector3 Vec0(-0.5f, 0.5f, 0);
		Vector3 Vec1(0.5f, 0.5f, -2.0);
		Vector3 Vec2(-0.5f, -0.5f, -2.0);
		Vec0 = Vector3EX::Transform(Vec0, mt);
		Vec1 = Vector3EX::Transform(Vec1, mt);
		Vec2 = Vector3EX::Transform(Vec2, mt);
		auto Cross = Vector3EX::Cross(Vec1 - Vec0, Vec2 - Vec0);
		Cross.Normalize();
		m_MyLight.SetPositionToDirectional(Cross);
	}
	DynamicPlate::~DynamicPlate() {}


	//初期化
	void DynamicPlate::OnCreate() {
		m_BackupVertices.clear();
		m_BackupIndices.clear();

		//メッシュと頂点変更できる描画コンポーネントの作成
		float HelfWidthCount = (float)m_WidthCount / 2.0f;
		float WidthPieceSize = 1.0f / (float)m_WidthCount;
		float HelfHeightCount = (float)m_HeightCount / 2.0f;
		float HeightPieceSize = 1.0f / (float)m_HeightCount;
		uint16_t SqCount = 0;
		float VCount = 0.0f;
		for (float y = HelfHeightCount; y > -HelfHeightCount; y -= 1.0f) {
			float UCount = 0.0f;
			for (float x = -HelfWidthCount; x < HelfWidthCount; x += 1.0f) {
				float PosX = WidthPieceSize * x;
				float PosY = HeightPieceSize * y;
				if (m_IsTile) {
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX, PosY, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(0.0f, 0.0f)));
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX + WidthPieceSize, PosY, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(1.0f, 0.0f)));
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX, PosY - HeightPieceSize, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(0.0f, 1.0f)));
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX + WidthPieceSize, PosY - HeightPieceSize, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(1.0f, 1.0f)));
				}
				else {
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX, PosY, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(UCount, VCount)));
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX + WidthPieceSize, PosY, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(UCount + WidthPieceSize, VCount)));
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX, PosY - HeightPieceSize, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(UCount, VCount + HeightPieceSize)));
					m_BackupVertices.push_back(VertexPositionNormalTexture(Vector3(PosX + WidthPieceSize, PosY - HeightPieceSize, 0), Vector3(0.0f, 0.0f, -1.0f), Vector2(UCount + WidthPieceSize, VCount + HeightPieceSize)));
				}
				m_BackupIndices.push_back(SqCount + 0);
				m_BackupIndices.push_back(SqCount + 1);
				m_BackupIndices.push_back(SqCount + 2);
				m_BackupIndices.push_back(SqCount + 1);
				m_BackupIndices.push_back(SqCount + 3);
				m_BackupIndices.push_back(SqCount + 2);
				SqCount += 4;
				UCount += WidthPieceSize;
			}
			VCount += HeightPieceSize;
		}
		auto PtrDraw = AddComponent<PNTDynamicDraw>();
		PtrDraw->CreateMesh(m_BackupVertices, m_BackupIndices);
		//更新用の頂点をバックアップと同じ内容にしておく
		m_UpdateVertices = m_BackupVertices;
		PtrDraw->SetTextureResource(m_TextureResName);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale);
		PtrTransform->SetQuaternion(m_StartQt);
		PtrTransform->SetPosition(m_StartPosition);
	}
	const Light& DynamicPlate::OnGetDrawLight()const {
		return m_MyLight;
	}

	PLANE DynamicPlate::GetPLANE() const {
		auto TrasnsPtr = GetComponent<Transform>();
		//表面上に3つの点を使ってPLANEを作成
		//1つ目の点は中心
		Vector3 Point0 = TrasnsPtr->GetPosition();
		//2つ目は-0.5,-0.5,0の点をワールド変換したもの
		Vector3 Point1(-0.5f, -0.5f, 0);
		Point1.Transform(TrasnsPtr->GetWorldMatrix());
		//3つ目は0.5,-0.5,0の点をワールド変換したもの
		Vector3 Point2(0.5f, -0.5f, 0);
		Point2.Transform(TrasnsPtr->GetWorldMatrix());
		//3点を使って面を作成
		PLANE ret(Point0, Point1, Point2);
		return ret;
	}

	COLRECT DynamicPlate::GetCOLRECT() const {
		auto TrasnsPtr = GetComponent<Transform>();
		COLRECT rect(1.0f, 1.0f, TrasnsPtr->GetWorldMatrix());
		return rect;
	}

	//点との最近接点を返す
	void DynamicPlate::ClosestPtPoint(const Vector3& Point, Vector3& Ret) {
		COLRECT rect = GetCOLRECT();
		Vector3 d = Point - rect.m_Center;
		Ret = rect.m_Center;
		for (int i = 0; i < 2; i++) {
			float dist = Vector3EX::Dot(d, rect.m_Rot[i]);
			if (dist > rect.m_UVec[i]) {
				dist = rect.m_UVec[i];
			}
			if (dist < -rect.m_UVec[i]) {
				dist = -rect.m_UVec[i];
			}
			Ret += rect.m_Rot[i] * dist;
		}
	}


	//点との距離を返す（戻り値がマイナスなら裏側）
	float DynamicPlate::GetDistPointPlane(const Vector3& Point) const {
		PLANE pl = GetPLANE();
		return (Vector3EX::Dot(Point, pl.m_Normal) - pl.m_DotValue) / Vector3EX::Dot(pl.m_Normal, pl.m_Normal);
	}

	//ヒットテストをして最近接点と、Squareの法線を返す
	bool DynamicPlate::HitTestSphere(const SPHERE& Sp, Vector3& RetVec, Vector3& Normal) {
		PLANE pl = GetPLANE();
		Normal = pl.m_Normal;
		//四角形との最近接点を得る
		ClosestPtPoint(Sp.m_Center, RetVec);
		//最近接点が半径以下なら衝突している
		if (Vector3EX::Length(Sp.m_Center - RetVec) <= Sp.m_Radius) {
			return true;
		}
		return false;
	}


	//--------------------------------------------------------------------------------------
	//	タイリングする普通の壁
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TileWall::TileWall(const shared_ptr<Stage>& StagePtr,
		wstring TextureResName,
		UINT WidthTileCount,
		UINT HeightTileCount,
		const Vector3& Scale,
		const Quaternion& Qt,
		const Vector3& Position
	) :
		DynamicPlate(StagePtr, TextureResName, WidthTileCount, HeightTileCount,
			true, Scale, Qt, Position),
		m_WidthTileCount(WidthTileCount),
		m_HeightTileCount(HeightTileCount)
	{

	}
	TileWall::~TileWall() {}

	void TileWall::OnCreate() {
		DynamicPlate::OnCreate();
		auto PtrDraw = GetComponent<PNTDynamicDraw>();
		PtrDraw->UpdateVertices(m_UpdateVertices);
		PtrDraw->SetWrapSampler(true);

	}
	//--------------------------------------------------------------------------------------
	//	スクエア
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TargetSquare::TargetSquare(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}
	TargetSquare::~TargetSquare() {}

	//初期化
	void TargetSquare::OnCreate() {

		auto PtrTransform = GetComponent<Transform>();
		auto SeekPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto SeekTransPtr = SeekPtr->GetComponent<Transform>();
		auto Pos = SeekTransPtr->GetPosition();
		Pos.y += 0.75f;
		PtrTransform->SetPosition(Pos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetQuaternion(SeekTransPtr->GetQuaternion());
		//変更できるスクエアリソースを作成

		//頂点配列
		vector<VertexPositionNormalTexture> vertices;
		//インデックスを作成するための配列
		vector<uint16_t> indices;
		//Squareの作成(ヘルパー関数を利用)
		MeshUtill::CreateSquare(1.0f, vertices, indices);
		//左上頂点
		vertices[0].textureCoordinate = Vector2(0.0, 0);
		//右上頂点
		vertices[1].textureCoordinate = Vector2(1.0, 0);
		//左下頂点
		vertices[2].textureCoordinate = Vector2(0.0, 1.0f);
		//右下頂点
		vertices[3].textureCoordinate = Vector2(1.0, 1.0f);
		//頂点の型を変えた新しい頂点を作成
		vector<VertexPositionColorTexture> new_vertices;
		for (auto& v : vertices) {
			VertexPositionColorTexture nv;
			nv.position = v.position;
			nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
			nv.textureCoordinate = v.textureCoordinate;
			new_vertices.push_back(nv);
		}
		//新しい頂点を使ってメッシュリソースの作成
		m_SquareMeshResource = MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true);

		auto DrawComp = AddComponent<PCTStaticDraw>();
		DrawComp->SetMeshResource(m_SquareMeshResource);
		DrawComp->SetTextureResource(L"TARGET_TX");
		SetAlphaActive(true);
		SetDrawActive(false);
		SetDrawLayer(1);

	}


	//変化
	void TargetSquare::OnUpdate() {

		auto SeekTransPtr = GetStage()->GetSharedGameObject<Player>(L"Player", false);
		auto PLTrans = SeekTransPtr->GetComponent<Transform>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto PtrTransform = GetComponent<Transform>();


		auto Pos = PLTrans->GetPosition();
		auto Rot = PLTrans->GetRotation();
		Pos.x += sin(Rot.y) *2;
		Pos.z += cos(Rot.y) *2;
		Rolling += ElapsedTime*5;
		PtrTransform->SetPosition(Pos);
		PtrTransform->SetRotation(XM_PIDIV2, Rolling, 0);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
	}
	//--------------------------------------------------------------------------------------
	//	class  : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ScoreItem::ScoreItem(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{}
	ScoreItem::~ScoreItem() {}
	//初期化
	void ScoreItem::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionSphere>();

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.5f, 0.5f, 0.5f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.25f, 0.0f)
		);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"NUTS_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		//AddComponent<Gravity>();

		auto PtrDraw = AddComponent<PNTStaticModelDraw>();
		PtrDraw->SetMeshResource(L"NUTS_MESH");
		//PtrDraw->SetDepthStencilState(DepthStencilState::Read); // スプライト多いモデルなどはこれを適用
		PtrDraw->SetMeshToTransformMatrix(SpanMat);
		PtrDraw->SetLighting(ShaderLighting::Midium);
		PtrDraw->SetEmissive(Color4(0.4, 0.4, 0.4, 0));
		PtrDraw->SetDiffuse(Color4(0.6, 0.6, 0.6, 1));
		SetAlphaActive(true);

		auto Act = AddComponent<Action>();
		Act->AddRotateBy(1.0f, Vector3(0.0f,10.0f,0.0f));
		Act->Run();
		Act->SetLooped(true);

	}
	void ScoreItem::OnUpdate() {
	}

	//
	//背景用のボール
	//-------------------------------------------------------
	BGball::BGball(const shared_ptr<Stage>& StagePtr):GameObject(StagePtr){}
	BGball::~BGball(){}
	void BGball::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(Vector3(100,50,100));
		PtrTransform->SetPosition(Vector3(0,0,0));

		////影をつける
		//auto ShadowPtr = AddComponent<Shadowmap>();
		//ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		//PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"WOODLAND_TX");
		//PtrDraw->SetDepthStencilState(DepthStencilState::Read);
		SetAlphaActive(true);
		SetDrawLayer(-1);
	}
	void BGball::OnUpdate() {
	}



}
//end basecross
