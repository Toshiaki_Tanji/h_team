/*!
@file Sprite.cpp
@brief スプライトの実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	スプライト
	//--------------------------------------------------------------------------------------
	Sprite::Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos, const size_t& LayerNum) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_LayerNum(LayerNum)
	{}

	Sprite::~Sprite() {}
	void Sprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横1個ずつ表示)
		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vector3(-HelfSize, HelfSize, 0),Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(-HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 1.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1, 1.0f), Vector2(1.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
		SetDrawLayer(m_LayerNum);
		SetAlphaActive(m_Trace);
		SetDrawLayer(10);
		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Get");
	}

	void Sprite::OnUpdate() {

	}
	//--------------------------------------------------------------------------------------
	///	スコア表示のスプライト
	//--------------------------------------------------------------------------------------
	NumSprite::NumSprite(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
		const Vector3& StartScale, const Vector3& StartPos, const wstring& HowTo) :
		GameObject(StagePtr),
		m_NumberOfDigits(NumberOfDigits),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_HowTo(HowTo)
	{}

	void NumSprite::OnCreate() {

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//スプライト付加
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"NUMBER_TX");
		//透過処理
		SetAlphaActive(true);
		//左上頂点
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//数字毎に分かれるよう頂点データを配列化
		for (size_t i = 0; i < 10; i++) {
			//←X座標
			float from = ((float)i) / 10.0f;
			//fromから→に数字一つ分飛ぶ
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVertex =
			{
				//左上頂点データ
				VertexPositionColorTexture(
					SpVertexVec[0].position,
					Color4(1.0f,1.0f,1.0f,1.0f),
					Vector2(from,0)
				),
				//右上頂点データ
				VertexPositionColorTexture(
					SpVertexVec[1].position,
					Color4(1.0f,1.0f,1.0f,1.0f),
					Vector2(to,0)
				),
				//左下頂点データ
				VertexPositionColorTexture(
					SpVertexVec[2].position,
					Color4(1.0f,1.0f,1.0f,1.0f),
					Vector2(from,1.0f)
				),
				//右下頂点データ
				VertexPositionColorTexture(
					SpVertexVec[3].position,
					Color4(1.0f,1.0f,1.0f,1.0f),
					Vector2(to,1.0f)
				),
			};
			m_BackupVertices.push_back(NumVertex);
		}
		SetDrawLayer(10);
	}

	void NumSprite::OnUpdate() {
		//
		if (m_HowTo == L"LIFE") {
			m_NumberOfDigits = App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife;
		}
		else if (m_HowTo == L"SCORE") {
			m_NumberOfDigits = App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_ScoreItem;
		}
		size_t Num = (size_t)m_NumberOfDigits;
		Num = Num % 10;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わるよう設定
		MeshRes->UpdateVirtexBuffer(m_BackupVertices[Num]);
	}

	//-------------------------------------------------------------------------------------
	//構築と破棄
	Fade::Fade(const shared_ptr<Stage>& StagePtr, const wstring& WorB, const wstring& INorOUT) :
		GameObject(StagePtr),
		m_WorB(WorB),
		m_INorOUT(INorOUT)
	{}

	//初期化
	void Fade::OnCreate() {

		float width = static_cast<float>(App::GetApp()->GetGameWidth());
		float height = static_cast<float>(App::GetApp()->GetGameHeight());

		float HelfSize = 0.5f;
		//頂点配列(縦横1個ずつ表示)
		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vector3(-HelfSize, HelfSize,  0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, HelfSize,  0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(-HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 1.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(width, height, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		if (m_WorB == L"W") {
			if (m_INorOUT == L"IN") {
				PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
			}
			else if (m_INorOUT == L"OUT") {
				PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 0.0f));
			}
		}
		else if (m_WorB == L"B") {
			if (m_INorOUT == L"IN") {
				PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 0.0f, 1.0f));
			}
			else if (m_INorOUT == L"OUT") {
				PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 0.0f, 0.0f));
			}
		}
		PtrDraw->SetTextureResource(L"FADE_TX");
		SetAlphaActive(true);
		SetDrawLayer(50);
	}

	void Fade::OnUpdate() {
		//頂点とインデックスを指定してスプライト作成
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		auto Fader = PtrDraw->GetDiffuse();
		if (m_WorB == L"W") {
			Fader.x = 1.0f; Fader.y = 1.0f, Fader.z = 1.0f;
			if (m_INorOUT == L"IN") {
				Fader.w -= ElapsedTime;
			}
			else if (m_INorOUT == L"OUT") {
				Fader.w += ElapsedTime;
			}
			if (Fader.w < 0.0f) {
				Fader.w = 0.0f;
			}
			else if (Fader.w > 1.0f) {
				Fader.w = 1.0f;
			}
			PtrDraw->SetDiffuse(Fader);
		}
		else if (m_WorB == L"B") {
			Fader.x = 0.0f; Fader.y = 0.0f, Fader.z = 0.0f;
			if (m_INorOUT == L"IN") {
				Fader.w -= ElapsedTime;
			}
			else if (m_INorOUT == L"OUT") {
				Fader.w += ElapsedTime;
			}
			if (Fader.w < 0.0f) {
				Fader.w = 0.0f;
			}
			else if (Fader.w > 1.0f) {
				Fader.w = 1.0f;
			}
			PtrDraw->SetDiffuse(Fader);
		}

	}
	wstring Fade::GetFader()const {
		return m_INorOUT;
	}
	void Fade::SetFader(const wstring& INorOUT) {
		m_INorOUT = INorOUT;
	}


}
//end basecross
