/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {


	//-------------------------------------------------------------------------
	///	TitleStage
	//-------------------------------------------------------------------------
	//ビューとライトの作成
	void TitleStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();

		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//壁模様のスプライト作成
	void TitleStage::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<Sprite>(L"TITLE_TX", false,
			Vector2(w, h), Vector2(0, 0),0);
		auto TitlePtr = AddGameObject<Sprite>(L"PUSH_A_TX", true,
			Vector2(512, 256), Vector2(0, -h/4), 0);
		AddGameObject<Sprite>(L"TITLELOGO_TX", true,
			Vector2(800, 800), Vector2(0, h/6), 0);

		SetSharedGameObject(L"PUSH_A",TitlePtr);
	}
	void TitleStage::CreateFader(){
		auto Ptr = AddGameObject<Fade>(L"W", L"IN");
		SetSharedGameObject(L"FADE",Ptr);
	}


	void TitleStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();
			CreateSprite();
			//
			CreateFader();

		}
		catch (...) {
			throw;
		}
	}
	void TitleStage::OnUpdate() {

		auto A = GetSharedGameObject<Sprite>(L"PUSH_A", true);
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		m_runTimer += ElapsedTime*5;
		auto PtrDraw = A->GetComponent<PCTSpriteDraw>();
		auto Light = PtrDraw->GetDiffuse();
		if (!PushBotton) {
			Light.w = sin(m_runTimer);
		}
		else {
			Light.w *= -1.0f;
		}
		PtrDraw->SetDiffuse(Light);

		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//Bボタンが押されたら.
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !PushBotton) {
			A->GetComponent<MultiSoundEffect>()->Start(L"Get", 0, 0.5f);
			auto v = GetSharedGameObject<Fade>(L"FADE");
			v->SetFader(L"OUT");
			PushBotton = true;
			//イベント送出
			PostEvent(1.0f, GetThis<TitleStage>(), App::GetApp()->GetSceneInterface(), L"ToSelect");
		}
	}
	//--------------------------------------------------------------------------------
	/// SelectStage
	//--------------------------------------------------------------------------------
	//ビューとライトの作成
	void SelectStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();

		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//壁模様のスプライト作成
	void SelectStage::CreateSprite() {

		auto SpriteGroup = CreateSharedObjectGroup(L"SelectSpriteGroup");
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		float size = 128;
		AddGameObject<Sprite>(L"SELECT_TX", false,
			Vector2(w, h), Vector2(0, 0), 0);

		AddGameObject<Sprite>(L"WOODFLAME_TX", true,
			Vector2(612, 768), Vector2(-w / 4, 0), 0);
		AddGameObject<Sprite>(L"SCROLL_TX", true,
			Vector2(768, 768), Vector2(w / 5, -h/10), 0);
		auto s = AddGameObject<Sprite>(L"STAGE1_TX", true,
			Vector2(size * 1.5f, size*1.5f), Vector2(-w / 4, 192), 0);
		SpriteGroup->IntoGroup(s);
		s = AddGameObject<Sprite>(L"STAGE2_TX", true,
			Vector2(size * 1.5f, size*1.5f), Vector2(-w / 4, 0), 0);
		SpriteGroup->IntoGroup(s);
		s = AddGameObject<Sprite>(L"STAGE3_TX", true,
			Vector2(size * 1.5f, size*1.5f), Vector2(-w / 4, -192), 0);
		SpriteGroup->IntoGroup(s);
		AddGameObject<Sprite>(L"SELECTMENU_TX", true,
			Vector2(768, 384), Vector2(w/5, h / 3), 0);

	}

	void SelectStage::CreateFader() {
		auto Ptr = AddGameObject<Fade>(L"W", L"IN");
		SetSharedGameObject(L"FADE", Ptr);
	}

	void SelectStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();
			CreateSprite();
			CreateFader();
		}
		catch (...) {
			throw;
		}
	}
	void SelectStage::OnUpdate() {
		int size = 128;
		auto sp = GetSharedObjectGroup(L"SelectSpriteGroup");
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			for (size_t i = 0; i < sp->size(); i++)
			{
				auto spn_Pos = sp->at(i)->GetComponent<Transform>()->GetPosition();
				if (i == StageNo) {
					sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(size*2.0f, size * 2.0f, 1.0f));
					//spn->at(i)->GetComponent<Transform>()->SetScale(Vector3(128  1.3f, 128  1.3f, 1.0f));
					//spn->at(i)->GetComponent<Transform>()->SetPosition(Vector3(200, spn_Pos.y, spn_Pos.z));
					continue;
				}
				sp->at(i)->GetComponent<Transform>()->SetScale(Vector3(size*1.5f, size*1.5f, 1.0f));
			}
			if (CntlVec[0].fThumbLY >= 0.7f && !PushBotton) {
				if (!OnePushSelect) {
					StageNo--;
					if (StageNo < 0) {
						StageNo = sp->size() - 1;
					}
					OnePushSelect = true;
				}
			}
			else if (CntlVec[0].fThumbLY <= -0.7f && !PushBotton) {
				if (!OnePushSelect) {
					StageNo++;
					if (StageNo > sp->size() - 1) {
						StageNo = 0;
					}
					OnePushSelect = true;
				}
			}
			else {
				OnePushSelect = false;
			}
			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A&& !PushBotton) {
				auto v = GetSharedGameObject<Fade>(L"FADE");
				v->SetFader(L"OUT");
				PushBotton = true;
				//イベント送出
				PostEvent(1.0f, GetThis<SelectStage>(), App::GetApp()->GetSceneInterface(), L"ToGame");
			}
		}
	}
	//--------------------------------------------------------------------------------
	/// ResultStage
	//--------------------------------------------------------------------------------
	//ビューとライトの作成
	void ResultStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();

		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}
	//壁模様のスプライト作成
	void ResultStage::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		auto Rank = App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife;
		if (Rank > 2) {
			AddGameObject<Sprite>(L"RESULT_S_TX", false,
				Vector2(w, h), Vector2(0, 0), 0);
			AddGameObject<Sprite>(L"S_TX", true,
				Vector2(512, 512), Vector2(256, 0), 1);
		}
		else if (Rank > 1) {
			AddGameObject<Sprite>(L"RESULT_A_TX", false,
				Vector2(w, h), Vector2(0, 0), 0);
			AddGameObject<Sprite>(L"A_TX", true,
				Vector2(512, 512), Vector2(256, 0), 1);
		}
		else if (Rank > 0) {
			AddGameObject<Sprite>(L"RESULT_B_TX", false,
				Vector2(w, h), Vector2(0, 0), 0);
			AddGameObject<Sprite>(L"B_TX", true,
				Vector2(512, 512), Vector2(256, 0), 1);
		}

		auto Ptr = AddGameObject<Sprite>(L"TOTITLE_TX", true,
			Vector2(512,512 ), Vector2(0, -h/3), 0);
		Ptr = AddGameObject<Sprite>(L"TORETRY_TX", true,
			Vector2(512,512 ), Vector2(256, -h/3), 0); 
		Ptr = AddGameObject<Sprite>(L"TONEXT_TX", true,
				Vector2(512,512), Vector2(512, -h/3), 0);

	}

	void ResultStage::CreateFader() {
		auto Ptr = AddGameObject<Fade>(L"W", L"IN");
		SetSharedGameObject(L"FADE", Ptr);
	}


	void ResultStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();
			CreateSprite();
			CreateFader();
		}
		catch (...) {
			throw;
		}
	}

	void ResultStage::OnUpdate() {

		

		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//Bボタンが押されたら.
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !PushBotton) {
			auto v = GetSharedGameObject<Fade>(L"FADE");
			v->SetFader(L"OUT");
			PushBotton = true;
			//イベント送出
			PostEvent(1.0f, GetThis<ResultStage>(), App::GetApp()->GetSceneInterface(), L"ToTitle");
		}
	}

	//--------------------------------------------------------------------------------
	/// ガメオベラ
	//--------------------------------------------------------------------------------
	//ビューとライトの作成
	void GameOver::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();

		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}
	//壁模様のスプライト作成
	void GameOver::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<Sprite>(L"GAMEOVER_TX", false,
			Vector2(w, h), Vector2(0, 0), 0);
	}

	void GameOver::CreateFader() {
		auto Ptr = AddGameObject<Fade>(L"B", L"IN");
		SetSharedGameObject(L"FADE", Ptr);
	}


	void GameOver::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();
			CreateSprite();
			CreateFader();
		}
		catch (...) {
			throw;
		}
	}

	void GameOver::OnUpdate() {
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//Bボタンが押されたら.
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !PushBotton) {
			auto v = GetSharedGameObject<Fade>(L"FADE");
			v->SetFader(L"OUT");
			PushBotton = true;
			//イベント送出
			PostEvent(1.0f, GetThis<GameOver>(), App::GetApp()->GetSceneInterface(), L"ToTitle");
		}
	}


}
//end basecross
