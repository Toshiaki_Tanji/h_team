/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------

	//ビューとライトの作成
	void GameStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//カメラの最遠設定
		PtrLookAtCamera->SetMaxArm(5.0f);
		//シングルライトの作成0.0f
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}



	//固定のモデルの作成
	void GameStage::CreateStaticModel() {
		for (int i = 0; i < 5; i++) {
			AddGameObject<StaticModel>(Vector3(0.5, 5.0, 0.5), Vector3(0, XM_PIDIV4 * i, 0), Vector3(-10.0f, 2.5f, -10.0f + i * 3), L"WOOD_MESH");
			AddGameObject<StaticModel>(Vector3(0.5, 5.0, 0.5), Vector3(0, XM_PIDIV4 * i, 0), Vector3(9.0f, 2.5f, -10.0f + i * 3), L"MALLONWOOD_MESH");
			AddGameObject<DamegeObj>(Vector3(0.5, 0.5, 0.5), Vector3(0, XM_PIDIV4 * i, 0), Vector3(8.5f, 1.5f, -10.0f + i * 3));
		}
	}



	//プレイヤーの作成
	void GameStage::CreatePlayer() {
		//プレーヤーの作成
		auto PlayerPtr = AddGameObject<Player>();
		//シェア配列にプレイヤーを追加
		SetSharedGameObject(L"Player", PlayerPtr);
		//ターゲットの円を作成
		auto TargetPtr = AddGameObject<TargetSquare>();
		SetSharedGameObject(L"Target", TargetPtr);

	}

	void GameStage::CreateTransBox() {

		auto Group = CreateSharedObjectGroup(L"TransBox");
		float sizeY = 0.0f;
		vector<Vector3> Pos;
		float startX = -10.0f;
		float startY = 1.5f;
		float startZ = -10.0f;
		srand((unsigned int)time(NULL));

		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				Pos.push_back(Vector3(startX + i, 0.5f, startZ + j));
			}
		}

		for (auto v : Pos) {
			auto Random = rand();
			auto Value = rand();
			switch (Value % 5) {
			case 0:
			case 1:
			case 2:
				AddGameObject<NormalBox>(
					Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), v);
				break;
			case 3:
			case 4:
				auto Ptr = AddGameObject<TransformBox>(
					Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), v);
				Group->IntoGroup(Ptr);
				break;

			}
		}
	}

	void GameStage::CreateNormBox() {

		AddGameObject<NormalBox>(
			Vector3(2.0f, 3.0f, 2.0f), Vector3(0, 0, 0), Vector3(-8.5f, 2.5, -8.5f));
		AddGameObject<NormalBox>(
			Vector3(4.0f, 5.0f, 4.0f), Vector3(0, 0, 0), Vector3(-5.5f, 3.5, -5.5f));
		AddGameObject<NormalBox>(
			Vector3(1.0f, 4.0f, 1.0f), Vector3(0, 0, 0), Vector3(3.0f, 3.0, 3.0f));
		AddGameObject<NormalBox>(
			Vector3(6.0f, 3.0f, 6.0f), Vector3(0, 0, 0), Vector3(-7.5f, 2.5, 6.5f));
		AddGameObject<NormalBox>(
			Vector3(4.0f, 8.0f, 4.0f), Vector3(0, 0, 0), Vector3(7.5f, 5.0, -8.5f));
		AddGameObject<NormalBox>(
			Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(7.5f, 3.5, 3.5f));
		AddGameObject<NormalBox>(
			Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(8.5f, 3.5, 3.5f));
		AddGameObject<NormalBox>(
			Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(7.5f, 3.5, 4.5f));
		AddGameObject<NormalBox>(
			Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(8.5f, 3.5, 4.5f));

		float startY = 1.5f;
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j <4; j++) {
				AddGameObject<TransformBox>
					(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(-10.0f + i, startY + j, -11.0f));
				AddGameObject<TransformBox>
					(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(-10.0f + i, startY + j, 10.0f));
				AddGameObject<TransformBox>
					(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(-11.0f, startY + j, -10.0f + i));
				AddGameObject<TransformBox>
					(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(10.0f, startY + j, -10.0f + i));
			}
			AddGameObject<NormalBox>
				(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(-10.0f + i, startY + 4.0f, -11.0f));
			AddGameObject<NormalBox>
				(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(-10.0f + i, startY + 4.0f, 10.0f));
			AddGameObject<NormalBox>
				(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(-11.0f, startY + 4.0f, -10.0f + i));
			AddGameObject<NormalBox>
				(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(10.0f, startY + 4.0f, -10.0f + i));
		}
	}

	void GameStage::CreateScoreItem() {
		//スコアアイテム作成
		AddGameObject<ScoreItem>(
			Vector3(0.5, 0.5, 0.5), Vector3(0, 0, 0), Vector3(3.0f, 5.0, 3.0f));
		AddGameObject<ScoreItem>(
			Vector3(0.5, 0.5, 0.5), Vector3(0, 0, 0), Vector3(0.5f, 1.0, 6.5f));
		AddGameObject<ScoreItem>(
			Vector3(0.5, 0.5, 0.5), Vector3(0, 0, 0), Vector3(7.5f, 9.0, -8.5f));
	}

	void GameStage::CreateGoal()
	{
		//ゴール作成
		auto GoalPtr = AddGameObject<Goal>(
			Vector3(4.0f, 2.0f, 4.0f),
			Vector3(0, 0, 0),
			Vector3(-7.5f, 5.0f, 6.5f));
		SetSharedGameObject(L"Goal", GoalPtr);
	}


	void GameStage::CreateBackGround() {
		//背景ボール作成
		AddGameObject<BGball>();
	}

	//壁の作成
	void GameStage::CreateTileWall() {

		auto Grpup = CreateSharedObjectGroup(L"WallGroup");
		auto match = 0.5f;

		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		auto Ptr = AddGameObject<TileWall>(
			L"BOX_TX",
			20, 20,
			Vector3(20.0f, 20.0f, 1.0f),
			Qt,
			Vector3(-match, 1.0f, 20.0f - match)
			);
		Ptr->AddComponent<CollisionRect>();
		Grpup->IntoGroup(Ptr);

	}

	void GameStage::CreateUI() {
		auto PLLIFE = App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife;
		//LIFE
		for (UINT i = 0; i < PLLIFE; i++) {
			AddGameObject<LIFE>(L"LIFE_TX", true, Vector2(96.0f, 96.0f), Vector2(-576.0f + 82.0f * i, 336.0f), 1, 1 + i);
		}
		//木の実
		AddGameObject<Sprite>(L"NUTS_TX", true, Vector2(110.0f, 110.0f), Vector2(320.0f, 336.0f), 0);
		//のこり
		AddGameObject<Sprite>(L"ITEM_TX", true, Vector2(256.0f, 256.0f), Vector2(502.0f, 336.0f), 0);
		//数字のスプライト
		AddGameObject<NumSprite>(3, Vector3(64.0f, 64.0f, 1.0f), Vector3(524, 336.0f, 1.0f), L"SCORE");
	}

	void GameStage::CreateFader() {
		AddGameObject<Fade>(L"W", L"IN");
	}

	void GameStage::OnCreate() {
		try {

			App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife = 3;
			App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_ScoreItem = 3;

			//ビューとライトの作成
			CreateViewLight();
			//背景の作成
			CreateBackGround();
			//壁の作成
			CreateTileWall();
			//固定のモデルの作成
			CreateStaticModel();
			//変化するボックスの作成
			CreateTransBox();
			//普通のボックス作成
			CreateNormBox();
			//ゴールの作成
			CreateGoal();
			//スコアアイテムの作成
			CreateScoreItem();
			//プレーヤーの作成
			CreatePlayer();
			//UIの作成
			CreateUI();
			//
			CreateFader();
			//砲弾用のシェアオブジェクトグループの作成
			CreateSharedObjectGroup(L"ShellBallGroup");
		}
		catch (...) {
			throw;
		}
	}
	void GameStage::OnUpdate() {
		auto Key = App::GetApp()->GetInputDevice().GetKeyState();
		//Bボタンが押されたら.
		if (Key.m_bPressedKeyTbl['R']) {
			//イベント送出
			PostEvent(0.0f, GetThis<GameStage>(), App::GetApp()->GetSceneInterface(), L"ToResult");
		}
		if (Key.m_bPressedKeyTbl['M']) {
			//ライフを減らす
			App::GetApp()->GetScene<Scene>()->GetGameParamaters().m_PlayerLife--;
		}
	}

}
//end basecross
