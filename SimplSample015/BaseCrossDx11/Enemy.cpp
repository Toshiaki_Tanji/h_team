
#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class Enemy : public GameObject;
	//	用途: 追いかける配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Enemy::Enemy(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_BaseY(m_StartPos.y),
		m_StateChangeSize(5.0f)
	{
	}
	Enemy::~Enemy() {}

	//初期化
	void Enemy::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(0.125f, 0.25f, 0.25f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		PtrRigid->SetReflection(0.5f);
		//Seek操舵
		auto PtrSeek = AddComponent<SeekSteering>();
		//Arrive操舵
		auto PtrArrive = AddComponent<ArriveSteering>();
		//Arriveは無効にしておく
		PtrArrive->SetUpdateActive(false);

		

		//オブジェクトのグループを得る
		auto Group = GetStage()->GetSharedObjectGroup(L"ObjectGroup");
		//グループに自分自身を追加
		Group->IntoGroup(GetThis<Enemy>());
		//分離行動をつける
		AddComponent<SeparationSteering>(Group);
		//Obbの衝突判定をつける
		auto PtrColl = AddComponent<CollisionObb>();
		//横部分のみ反発
		PtrColl->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"TRACE_TX");
		//透明処理をする
		SetAlphaActive(true);

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Enemy> >(GetThis<Enemy>());
		//最初のステートをSeekFarStateに設定
		//m_StateMachine->ChangeState(FarState::Instance());
	}


	//ユーティリティ関数群
	Vector3 Enemy::GetPlayerPosition() const {
		//もしプレイヤーが初期化化されてない場合には、Vector3(0,m_BaseY,0)を返す
		Vector3 PlayerPos(0, m_BaseY, 0);
		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player", false);
		if (PtrPlayer) {
			PlayerPos = PtrPlayer->GetComponent<Transform>()->GetPosition();
			PlayerPos.y = m_BaseY;
		}
		return PlayerPos;
	}
	float Enemy::GetPlayerLength() const {
		auto MyPos = GetComponent<Transform>()->GetPosition();
		auto LenVec = GetPlayerPosition() - MyPos;
		return LenVec.Length();
	}

	//モーションを実装する関数群
	void  Enemy::SeekStartMoton() {
		auto PtrSeek = GetComponent<SeekSteering>();
		PtrSeek->SetUpdateActive(true);
		PtrSeek->SetTargetPosition(GetPlayerPosition());

	}
	bool  Enemy::SeekUpdateMoton() {
		auto PtrSeek = GetComponent<SeekSteering>();
		PtrSeek->SetTargetPosition(GetPlayerPosition());
		if (GetPlayerLength() <= m_StateChangeSize) {
			return true;
		}
		return false;
	}
	void  Enemy::SeekEndMoton() {
		auto PtrSeek = GetComponent<SeekSteering>();
		PtrSeek->SetUpdateActive(false);
	}

	void  Enemy::ArriveStartMoton() {
		auto PtrArrive = GetComponent<ArriveSteering>();
		PtrArrive->SetUpdateActive(true);
		PtrArrive->SetTargetPosition(GetPlayerPosition());
	}
	bool  Enemy::ArriveUpdateMoton() {
		auto PtrArrive = GetComponent<ArriveSteering>();
		PtrArrive->SetTargetPosition(GetPlayerPosition());
		if (GetPlayerLength() > m_StateChangeSize) {
			//プレイヤーとの距離が一定以上ならtrue
			return true;
		}
		return false;
	}
	void  Enemy::ArriveEndMoton() {
		auto PtrArrive = GetComponent<ArriveSteering>();
		//Arriveコンポーネントを無効にする
		PtrArrive->SetUpdateActive(false);
	}

	//操作
	void Enemy::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

	}
	void Enemy::OnLastUpdate() {
		//auto PtrRigidbody = GetComponent<Rigidbody>();
		////回転の更新
		////Velocityの値で、回転を変更する
		////これで進行方向を向くようになる
		//auto PtrTransform = GetComponent<Transform>();
		//Vector3 Velocity = PtrRigidbody->GetVelocity();
		//if (Velocity.Length() > 0.0f) {
		//	Vector3 Temp = Velocity;
		//	Temp.Normalize();
		//	float ToAngle = atan2(Temp.x, Temp.z);
		//	Quaternion Qt;
		//	Qt.RotationRollPitchYaw(0, ToAngle, 0);
		//	Qt.Normalize();
		//	//現在の回転を取得
		//	Quaternion NowQt = PtrTransform->GetQuaternion();
		//	//現在と目標を補間（10分の1）
		//	NowQt.Slerp(NowQt, Qt, 0.1f);
		//	PtrTransform->SetQuaternion(NowQt);
		//}
		////常にyはm_BaseY
		//auto Pos = PtrTransform->GetPosition();
		//Pos.y = m_BaseY;
		//PtrTransform->SetPosition(Pos);
	}


}
