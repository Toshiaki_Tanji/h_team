
#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メディアリソース作成
	//--------------------------------------------------------------------------------------

	void CResource::OnCreate() {
		
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		//テクスチャ登録
		wstring strTexture = DataDir + L"Earth.png";//L"Texture\\Ground.png";
		App::GetApp()->RegisterTexture(L"BOX_TX", strTexture);
		strTexture = DataDir + L"Texture\\Cave.png";
		App::GetApp()->RegisterTexture(L"CAVE_TX", strTexture);	
		strTexture = DataDir + L"Texture\\fader.png";
		App::GetApp()->RegisterTexture(L"FADE_TX", strTexture);
		strTexture = DataDir + L"Texture\\GAME_OVER.png";
		App::GetApp()->RegisterTexture(L"GAMEOVER_TX", strTexture);
		strTexture = DataDir + L"Texture\\Remaining.png";
		App::GetApp()->RegisterTexture(L"ITEM_TX", strTexture);		
		strTexture = DataDir + L"Grassnguri.png";//L"Texture\\Leaf.png";
		App::GetApp()->RegisterTexture(L"LEAF_TX", strTexture);	
		strTexture = DataDir + L"Texture\\heart_ON.png";
		App::GetApp()->RegisterTexture(L"LIFE_TX", strTexture);
		strTexture = DataDir + L"Texture\\heart_OFF.png";
		App::GetApp()->RegisterTexture(L"LIFELOSE_TX", strTexture);
		strTexture = DataDir + L"Texture\\number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = DataDir + L"Texture\\PushAbutton2.png";//Push_A_button.png";
		App::GetApp()->RegisterTexture(L"PUSH_A_TX", strTexture);
		strTexture = DataDir + L"Texture\\newresultbg.png";
		App::GetApp()->RegisterTexture(L"RESULT_TX", strTexture);
		strTexture = DataDir + L"Texture\\RESULT_S.png";
		App::GetApp()->RegisterTexture(L"RESULT_S_TX", strTexture);
		strTexture = DataDir + L"Texture\\RESULT_A.png";
		App::GetApp()->RegisterTexture(L"RESULT_A_TX", strTexture);
		strTexture = DataDir + L"Texture\\RESULT_B.png";
		App::GetApp()->RegisterTexture(L"RESULT_B_TX", strTexture);
		strTexture = DataDir + L"Texture\\StageSelect3.png";
		App::GetApp()->RegisterTexture(L"SCROLL_TX", strTexture);
		strTexture = DataDir + L"Texture\\select.png";
		App::GetApp()->RegisterTexture(L"SELECT_TX", strTexture);
		strTexture = DataDir + L"Texture\\StageSelect4.png";
		App::GetApp()->RegisterTexture(L"SELECTMENU_TX", strTexture);
		strTexture = DataDir + L"Texture\\sky_box.png";
		App::GetApp()->RegisterTexture(L"SKYBOX_TX", strTexture);
		strTexture = DataDir + L"Texture\\Stage1.png";
		App::GetApp()->RegisterTexture(L"STAGE1_TX", strTexture);
		strTexture = DataDir + L"Texture\\Stage2.png";
		App::GetApp()->RegisterTexture(L"STAGE2_TX", strTexture);
		strTexture = DataDir + L"Texture\\Stage3.png";
		App::GetApp()->RegisterTexture(L"STAGE3_TX", strTexture);
		strTexture = DataDir + L"Texture\\Stage4.png";
		App::GetApp()->RegisterTexture(L"STAGE4_TX", strTexture);
		strTexture = DataDir + L"Texture\\Stage5.png";
		App::GetApp()->RegisterTexture(L"STAGE5_TX", strTexture);
		strTexture = DataDir + L"Texture\\target_1.png";
		App::GetApp()->RegisterTexture(L"TARGET_TX", strTexture);		
		strTexture = DataDir + L"Texture\\TITLE_background.png";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);	
		strTexture = DataDir + L"Texture\\GameTitleName2.png";//Game_Title.png";
		App::GetApp()->RegisterTexture(L"TITLELOGO_TX", strTexture);
		strTexture = DataDir + L"Texture\\Tree_nuts.png";
		App::GetApp()->RegisterTexture(L"NUTS_TX", strTexture);
		strTexture = DataDir + L"Texture\\wall.png";
		App::GetApp()->RegisterTexture(L"WALL_TX", strTexture);
		strTexture = DataDir + L"Texture\\StageSelect2-2.png";
		App::GetApp()->RegisterTexture(L"WOODFLAME_TX", strTexture);
		strTexture = DataDir + L"Texture\\forest2_Nowborn2.png";
		App::GetApp()->RegisterTexture(L"WOODLAND_TX", strTexture);

		strTexture = DataDir + L"Texture\\S.png";
		App::GetApp()->RegisterTexture(L"S_TX", strTexture);
		strTexture = DataDir + L"Texture\\A.png";
		App::GetApp()->RegisterTexture(L"A_TX", strTexture);
		strTexture = DataDir + L"Texture\\B.png";
		App::GetApp()->RegisterTexture(L"B_TX", strTexture);

		strTexture = DataDir + L"Texture\\NEXT.png";
		App::GetApp()->RegisterTexture(L"TONEXT_TX", strTexture);
		strTexture = DataDir + L"Texture\\TITLE.png";
		App::GetApp()->RegisterTexture(L"TOTITLE_TX", strTexture);
		strTexture = DataDir + L"Texture\\RETRY.png";
		App::GetApp()->RegisterTexture(L"TORETRY_TX", strTexture);

		//バックグラウンドミュージック登録
		wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\tw078_m.wav";
		App::GetApp()->RegisterWav(L"ODAYAKA", strMusic);
		strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\tw079_m.wav";
		App::GetApp()->RegisterWav(L"NONKI", strMusic);
		 strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\SpaceCave_m.wav";
		App::GetApp()->RegisterWav(L"DOUKUTSU", strMusic);
		strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\eth10_m.wav";
		App::GetApp()->RegisterWav(L"TOBIDASE", strMusic);
		strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\norinori_m.wav";
		App::GetApp()->RegisterWav(L"NORI", strMusic);
		strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\jazznight_m.wav";
		App::GetApp()->RegisterWav(L"NIGHT", strMusic);
		strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\sim_m.wav";
		App::GetApp()->RegisterWav(L"SIM", strMusic);
		strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\loop_109.wav";
		App::GetApp()->RegisterWav(L"KEIKAKU", strMusic);
		strMusic = App::GetApp()->m_wstrRelativeDataPath + L"BGM\\loop_4.wav";
		App::GetApp()->RegisterWav(L"YATTA", strMusic);
		//サウンドエフェクト登録
		wstring CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\cursor7.wav";
		App::GetApp()->RegisterWav(L"Cursor", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\nyu2.wav";
		App::GetApp()->RegisterWav(L"jump", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\jingle01.wav";
		App::GetApp()->RegisterWav(L"jingle", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\jingle04.wav";
		App::GetApp()->RegisterWav(L"jingle_2", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\se_crash_1.wav";
		App::GetApp()->RegisterWav(L"bakuha", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\decision5.wav";
		App::GetApp()->RegisterWav(L"Get", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\j_25.wav";
		App::GetApp()->RegisterWav(L"Arank", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\j-11.wav";
		App::GetApp()->RegisterWav(L"Brank", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\muci_fan_10.wav";
		App::GetApp()->RegisterWav(L"Srank", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"SE\\battle11.wav";
		App::GetApp()->RegisterWav(L"Sting", CursorWav);
		//アニメ入りモデル登録
		auto ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"character_animation.bmf");
		App::GetApp()->RegisterResource(L"PLAYER_MESH", ModelMesh);
		//スタティックモデル登録
		auto StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Grass.bmf");
		App::GetApp()->RegisterResource(L"GRASS_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"HOUSE.bmf");
		App::GetApp()->RegisterResource(L"HOUSE_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Mallon.bmf");
		App::GetApp()->RegisterResource(L"MALLON_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Wood.bmf");
		App::GetApp()->RegisterResource(L"WOOD_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Tree.bmf");
		App::GetApp()->RegisterResource(L"MALLONWOOD_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Branch.bmf");
		App::GetApp()->RegisterResource(L"NUTS_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"kuri_Gree.bmf");
		App::GetApp()->RegisterResource(L"GRRENMALLON_MESH", StaticModelMesh);

	};
}
