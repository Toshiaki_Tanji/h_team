/*!
@file BoxCreate.cpp
@brief ボックスの実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class TransformBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TransformBox::TransformBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	TransformBox::~TransformBox() {}

	//初期化
	void TransformBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);
		m_ScaleDefault = m_Scale;

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		//PtrObb->SetDrawActive(true);
		PtrObb->SetFixed(true);
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");
		//描画
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"BOX_TX");
		//シェアオブジェクトグループ化
		auto Group = GetStage()->GetSharedObjectGroup(L"TransBox");
		Group->IntoGroup(GetThis<GameObject>());

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<TransformBox> >(GetThis<TransformBox>());
		//最初のステートをFiringStateに設定
		m_StateMachine->SetCurrentState(OriginState::Instance());
		//FiringStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<TransformBox>());

	}

	void TransformBox::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();
		////ステートマシンを使うことでUpdate処理を分散できる		

	}

	void TransformBox::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		//for (auto &v : OtherVec) {
		//	auto shBomb = dynamic_pointer_cast<Bomb>(v);
		//	auto sh = dynamic_pointer_cast<GameObject>(v);
		//	if (shBomb) {
		//	}
		//	return;
		//}
	}

	void TransformBox::SetHitPos(Vector3 hitpos) {
		//if (m_LatestHitPos) {
		m_LatestHitPos = hitpos;
		//}
	}

	//伸びるモーション
	void TransformBox::TransformMotion() {
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Trans = GetComponent<Transform>();
		auto NowScale = Trans->GetScale();
		auto BoxPos = Trans->GetPosition();
		auto HitPos = m_LatestHitPos;
		auto LimitLong = 15.0f;
		ChangeTimer += ElapsedTime;
		//最初に上方向に当たった時に上に伸びるフラグ
		if (HitPos.y > BoxPos.y + NowScale.y / 2 && IsNSside == false && IsWEside == false) {
			IsUpper = true;
		}
		//最初に右左方向に当たった時に横に伸びるフラグ
		else if ((HitPos.x > BoxPos.x + NowScale.x / 2 || HitPos.x < BoxPos.x - NowScale.x / 2)
			&& IsNSside == false && IsUpper == false) {
			IsWEside = true;
		}
		//最初に手前奥方向に当たった時に奥に伸びるフラグ
		else if ((HitPos.z > BoxPos.z + NowScale.z / 2 || HitPos.z < BoxPos.z - NowScale.z / 2)
			&& IsWEside == false && IsUpper == false) {
			IsNSside = true;
		}
		//どれでもないときに変形してなかったとき、設定を戻す
		else if (IsTransed == false) {
			m_LatestHitPos = Vector3(0.0f, 0.0f, 0.0f);
		}
		//上方向に伸びる
		if (IsUpper) {
			//変形はじめ
			if (ChangeTimer < 1.0f && NowScale.y < LimitLong) {
				NowScale.y += ElapsedTime * 4;
				Trans->SetScale(NowScale);
			}
			//変形終わり
			else {
				ChangeTimer = 0.0f;
				GetStateMachine()->ChangeState(ReturnState::Instance());
				IsTransed = true;
			}
		}
		//奥行方向に伸びる
		if (IsNSside) {
			//変形はじめ
			if (ChangeTimer < 1.0f && NowScale.z < LimitLong) {
				NowScale.z += ElapsedTime * 4;
				Trans->SetScale(NowScale);
			}
			//変形終わり
			else {
				ChangeTimer = 0.0f;
				GetStateMachine()->ChangeState(ReturnState::Instance());
				IsTransed = true;
			}
		}
		//横方向に伸びる
		if (IsWEside) {
			//変形はじめ
			if (ChangeTimer < 1.0f && NowScale.x < LimitLong) {
				NowScale.x += ElapsedTime * 4;
				Trans->SetScale(NowScale);
			}
			//変形終わり
			else {
				ChangeTimer = 0.0f;
				GetStateMachine()->ChangeState(ReturnState::Instance());
				IsTransed = true;
			}
		}
	}

	//元に戻るモーション
	void TransformBox::ReturnMotion() {
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Trans = GetComponent<Transform>();
		auto NowScale = Trans->GetScale();

		if (IsTransed) {
			//Yスケールを元に戻す
			if (NowScale.y > m_Scale.y) {
				NowScale.y -= ElapsedTime;
				Trans->SetScale(NowScale);
			}
			//Xスケールを元に戻す
			else if (NowScale.x > m_Scale.x) {
				NowScale.x -= ElapsedTime;
				Trans->SetScale(NowScale);
			}
			//Zスケールを元に戻す
			else if (NowScale.z > m_Scale.z) {
				NowScale.z -= ElapsedTime;
				Trans->SetScale(NowScale);
			}
			//変形終了したら返す
			else {
				IsTransed = false;
			}
		}
	}

	bool TransformBox::IsReturned() {
		if (IsTransed == false) {
			auto Trans = GetComponent<Transform>();
			Trans->SetScale(m_ScaleDefault);
			Trans->SetPosition(m_Position);
			m_LatestHitPos = Vector3(0.0f, 0.0f, 0.0f);
			IsUpper = false;
			IsNSside = false;
			IsWEside = false;
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------------------
	//	class OriginState : public ObjState<TransformBox>;
	//	用途: 通常ステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<OriginState> OriginState::Instance() {
		static shared_ptr<OriginState> instance;
		if (!instance) {
			instance = shared_ptr<OriginState>(new OriginState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void OriginState::Enter(const shared_ptr<TransformBox>& Obj) {
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void OriginState::Execute(const shared_ptr<TransformBox>& Obj) {
	}
	//ステートにから抜けるときに呼ばれる関数
	void OriginState::Exit(const shared_ptr<TransformBox>& Obj) {
		//何もしない
	}
	//--------------------------------------------------------------------------------------
	//	class TransUPState : public ObjState<TransformBox>;
	//	用途:変形するステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<TransUPState> TransUPState::Instance() {
		static shared_ptr<TransUPState> instance;
		if (!instance) {
			instance = shared_ptr<TransUPState>(new TransUPState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void TransUPState::Enter(const shared_ptr<TransformBox>& Obj) {
		//開始
	}
	//ステート実行中に毎ターン呼ばれる関数
	void TransUPState::Execute(const shared_ptr<TransformBox>& Obj) {
		//実行
		Obj->TransformMotion();
	}
	//ステートにから抜けるときに呼ばれる関数
	void TransUPState::Exit(const shared_ptr<TransformBox>& Obj) {
		//何もしない
	}
	//--------------------------------------------------------------------------------------
	//	class ReturnState : public ObjState<TransformBox>;
	//	用途: 元に戻るステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ReturnState> ReturnState::Instance() {
		static shared_ptr<ReturnState> instance;
		if (!instance) {
			instance = shared_ptr<ReturnState>(new ReturnState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ReturnState::Enter(const shared_ptr<TransformBox>& Obj) {
		//開始
		Obj->ReturnMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void ReturnState::Execute(const shared_ptr<TransformBox>& Obj) {
		//実行
		Obj->ReturnMotion();
		if (Obj->IsReturned()) {
			Obj->GetStateMachine()->ChangeState(OriginState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void ReturnState::Exit(const shared_ptr<TransformBox>& Obj) {
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class  : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	NormalBox::NormalBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{}
	NormalBox::~NormalBox() {}
	//初期化
	void NormalBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"LEAF_TX");
		SetAlphaActive(true);

	}
}
//end basecross
