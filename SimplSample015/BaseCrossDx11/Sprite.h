/*!
@file Sprite.h
@brief 表示するスプライトなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	壁模様のスプライト
	//--------------------------------------------------------------------------------------
	class Sprite : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		wstring m_TextureKey;
		size_t m_LayerNum;
	public:
		Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos, const size_t& LayerNum);
		virtual ~Sprite();
		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};
	//--------------------------------------------------------------------------------------
	///	数字スプライト
	//--------------------------------------------------------------------------------------
	class NumSprite : public GameObject {
		Vector3 m_StartScale;
		Vector3 m_StartPos;
		//桁数
		UINT m_NumberOfDigits;
		wstring m_HowTo;
		//バックアップ頂点データ
		vector<vector<VertexPositionColorTexture> > m_BackupVertices;
	public:
		NumSprite(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const Vector3& StartScale, const Vector3& StartPos, const wstring& HowTo);
		virtual ~NumSprite() {}
		virtual void OnCreate() override;
		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	//	class Fade : public GameObject;
	//	用途: フェード用
	//--------------------------------------------------------------------------------------
	class Fade : public GameObject {
		wstring m_WorB;
		wstring m_INorOUT;
	public:
		//構築と破棄
		Fade(const shared_ptr<Stage>& StagePtr, const wstring& WorB, const wstring& INorOUT);
		virtual ~Fade() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;

		wstring GetFader()const;
		void SetFader(const wstring& INorOUT);

	};


}
//end basecross
