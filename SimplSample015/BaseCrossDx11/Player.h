/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{



	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject {
		shared_ptr< StateMachine<Player> >  m_StateMachine;	//ステートマシーン
															//移動の向きを得る
		Vector3 GetAngle();
		//最高速度
		float m_MaxSpeed;
		//減速率
		float m_Decel;
		//質量
		float m_Mass;

		float PIDIV8 = XM_PI/8.0f;
		float PI3DIV8 = XM_PI * 3.0f/8.0f;
		float PI5DIV8 = XM_PI * 5.0f/8.0f;
		float PI3DIV4 = XM_PIDIV4 * 3.0f;
		float PI7DIV8 = XM_PI * 7.0f/8.0f;

		bool isBombThrow;
		float CoolTime;

		bool m_IsOnScleMin;


	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr);
		virtual ~Player() {}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Player> > GetStateMachine() const {
			return m_StateMachine;
		}
		//モーションを実装する関数群
		//移動して向きを移動方向にする
		void MoveRotationMotion();
		//Aボタンでジャンプするどうかを得る
		bool IsJumpMotion();
		//Aボタンでジャンプする瞬間の処理
		void JumpMotion();
		//Aボタンでジャンプしている間の処理
		//ジャンプ終了したらtrueを返す
		bool JumpMoveMotion();
		//Xボタンで投げるるどうかを得る
		bool IsBombThrowMotion();
		//Xボタンで投げる瞬間の処理
		void BombThrowMotion();
		//Xボタンで投げている間の処理
		//投げ終ったらtrueを返す
		bool BombThrowEndMotion();

		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;
		
	};

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class DefaultState : public ObjState<Player>
	{
		DefaultState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class JumpState : public ObjState<Player>;
	//	用途: ジャンプ状態
	//--------------------------------------------------------------------------------------
	class JumpState : public ObjState<Player>
	{
		JumpState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<JumpState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	//	class BombThrow : public ObjState<Player>;
	//	用途: 爆弾投げ
	//--------------------------------------------------------------------------------------
	class BombThrow : public ObjState<Player>
	{
		BombThrow() {};
	public:
		//ステートのインスタンス取得
		static shared_ptr<BombThrow>Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートから抜ける時に呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;

	};


	class LIFE : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		wstring m_TextureKey;
		size_t m_LayerNum;
		UINT m_HPLen;
		shared_ptr< StateMachine<LIFE> >  m_StateMachine;	//ステートマシーン
	public:
		LIFE(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos, const size_t& LayerNum,const UINT& HPLen);
		virtual ~LIFE();
		virtual void OnCreate() override;
		virtual void OnUpdate()override;

	};

}
//end basecross

