/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage {
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		//void CreatePlate();
		//背景
		void CreateBackGround();
		//壁の作成
		void CreateTileWall();
		//固定のモデルの作成
		void CreateStaticModel();
		//変形するボックスの作成
		void CreateTransBox();
		//普通のボックスを作成
		void CreateNormBox();
		//スコアアイテムを作成
		void CreateScoreItem();
		//ゴールを作成
		void CreateGoal();
		//プレイヤーの作成
		void CreatePlayer();
		//UIを作成
		void CreateUI();
		//フェーダー
		void CreateFader();

	public:
		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};


}
//end basecross
