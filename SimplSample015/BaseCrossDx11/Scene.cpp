
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------

	void Scene::OnCreate(){
		try {

			//リソースの作成
			m_resource = ObjectFactory::Create<CResource>();			

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"DOUKUTSU");
			m_AudioObjectPtr->AddAudioResource(L"NIGHT");
			m_AudioObjectPtr->AddAudioResource(L"NONKI");
			m_AudioObjectPtr->AddAudioResource(L"NORI");
			m_AudioObjectPtr->AddAudioResource(L"ODAYAKA");
			m_AudioObjectPtr->AddAudioResource(L"SIM");
			m_AudioObjectPtr->AddAudioResource(L"TOBIDASE");
			m_AudioObjectPtr->AddAudioResource(L"KEIKAKU");
			m_AudioObjectPtr->AddAudioResource(L"YATTA");
			//最初のBGMを再生する
			m_AudioObjectPtr->Start(L"TOBIDASE", XAUDIO2_LOOP_INFINITE, 1.0f);
			//いまのBGMを登録する
			m_Prebgm = L"TOBIDASE";
			//最初のアクティブステージの設定
			ResetActiveStage<TitleStage>();
		}
		catch (...) {
			throw;
		}
	}


	void Scene::OnEvent(const shared_ptr<Event>& event)
	{
		///タイトルへ
		if (event->m_MsgStr == L"ToTitle") {
			//BGM停止
			m_AudioObjectPtr->Stop(m_Prebgm);
			//BGM開始
			m_AudioObjectPtr->Start(L"TOBIDASE", XAUDIO2_LOOP_INFINITE, 1.0f);
			//いまのBGMを登録
			m_Prebgm = L"TOBIDASE";
			//最初のアクティブステージの設定
			ResetActiveStage<TitleStage>();
		}
		///セレクトへ
		else if (event->m_MsgStr == L"ToSelect") {
			//BGM停止
			m_AudioObjectPtr->Stop(m_Prebgm);
			//BGM開始
			m_AudioObjectPtr->Start(L"KEIKAKU", XAUDIO2_LOOP_INFINITE, 0.5f);
			//いまのBGMを登録
			m_Prebgm = L"KEIKAKU";

			//最初のアクティブステージの設定
			ResetActiveStage<SelectStage>();
		}
		///ゲームへ
		else if (event->m_MsgStr == L"ToGame") {
			//BGM停止
			m_AudioObjectPtr->Stop(m_Prebgm);
			//BGM開始
			m_AudioObjectPtr->Start(L"ODAYAKA", XAUDIO2_LOOP_INFINITE, 1.0f);
			//いまのBGMを登録
			m_Prebgm = L"ODAYAKA";

			//最初のアクティブステージの設定
			ResetActiveStage<GameStage>();
		}
		///リザルトへ
		else if (event->m_MsgStr == L"ToResult") {
			//BGM停止
			m_AudioObjectPtr->Stop(m_Prebgm);
			if (m_GameParamaters.m_PlayerLife >2) {
				//BGM開始
				m_AudioObjectPtr->Start(L"YATTA", XAUDIO2_LOOP_INFINITE, 0.2f);
				//いまのBGMを登録
				m_Prebgm = L"YATTA";
			}
			else if(m_GameParamaters.m_PlayerLife >1) {
				//BGM開始
				m_AudioObjectPtr->Start(L"NONKI", XAUDIO2_LOOP_INFINITE, 1.0f);
				//いまのBGMを登録
				m_Prebgm = L"NONKI";
			}
			else if (m_GameParamaters.m_PlayerLife >0) {
				//BGM開始
				m_AudioObjectPtr->Start(L"SIM", XAUDIO2_LOOP_INFINITE, 1.0f);
				//いまのBGMを登録
				m_Prebgm = L"SIM";
			}

			ResetActiveStage<ResultStage>();
		}
		///ガメオベラへ
		else if (event->m_MsgStr == L"ToGameOver") {
			//BGM停止
			m_AudioObjectPtr->Stop(m_Prebgm);
			//BGM開始
			m_AudioObjectPtr->Start(L"NIGHT", XAUDIO2_LOOP_INFINITE, 1.0f);
			//いまのBGMを登録
			m_Prebgm = L"NIGHT";

			ResetActiveStage<GameOver>();
		}

	}

}
//end basecross
